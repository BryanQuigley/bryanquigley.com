<html><body><p>I have a theory and I would like your help to disprove it (like all good science, aim to disprove it first).

The basic theory is that there are very few computers in one of these groups that is also not in the other:
</p><ul>
	<li>64 Bit Capable hardware</li>
	<li>Machines that can run Ubuntu/Unity</li>
</ul>
Or in other words:  If you can run Ubuntu with Unity you almost definitely have 64 bit capable hardware.  And, if you have 64 bit hardware you can run Ubuntu with Unity.

<h1>Help prove me wrong!  Answer the following questions (only submit if you answer yes to either):</h1>
If you can't see the form below, <a href="http://bryanquigley.com/?p=1762">click here</a>.

<iframe src="https://docs.google.com/forms/d/1LP5JB2MwyeUeHbvJzuxJgrcDdhAlVWhfOpUWywQljpg/viewform?embedded=true" height="500" width="760" frameborder="0" marginwidth="0" marginheight="0"></iframe></body></html>