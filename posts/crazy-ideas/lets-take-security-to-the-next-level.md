<html><body><blockquote><span style="font-size: large;">Current Setup:</span>
An application has to be limited by the most lax permission in order to maintain the functionality.  For an application that will ever have access to the user's files this means it needs to have access to all of the users files.

<span style="font-size: large;">Possible Solution:</span>
Have the file browser/chooser application give temporary permissions for the specific chosen files/folder to the application that launched the file chooser.  Care will need to be taken so that "recent files" in applications still work as expected.  This may require a per application recent file list to be stored in the security system.

Example Use Cases / How it does it:
Picture Viewer
1) User clicks on Picture with an active exploit in it (on the desktop)
2) Opens with default photo viewer
3) The exploit now has full control of the photo viewer, but can only access:
Photo viewers recently opened photos
The photo with the exploit
Photo viewer config
Anything else the photo viewer can access (say uploading to flickr)
All other photo's in library (if configured, which in this example it is not)
*) All other documents remain secure...

How it did it. (behind the scenes):
the user opened 4 pictures from the file manager, the application had those 4 pictures added to it's "per application recent file permission list" thereby enabling the user to open them directly from the photo manager at any point in the future.  That list was customized for the application to limit the list to the 4 most recent due to the application only having 4 option in it's "recent list".
This list is used by apparmor/selinux to enable access to the pictures for the application.

Rhythmbox
1) User configures library (using directory chooser)\
- Called with options to set up a permanent user/application permission for the music folder in question
- this allows rhythmbox to access all files contained within
2) User listens to internet radio and finds a malicious file
3) The malicious file deletes everything it can touch, the user loses her entire music collection, but has all documents intact.

When configuring the rhythmbox library directory, Rhythmbox used a special call to the directory chooser to ask it to switch it's permanent directory to whatever the user chooses, thereby adding the necessary rules as well.

Of course, if you can already do this with selinux/apparmor (at about the same complication level) please tell me how :)</blockquote>
<strong>Comments</strong>

 
<ul>
 	<li id="comment-2007" class="comment even thread-even depth-1"><article id="div-comment-2007" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Dylan McCall</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-01-21T17:35:09+00:00"> January 21, 2010 at 5:35 pm </time></div>
</footer>
<div class="comment-content">

&gt;Here you go: <a href="http://plash.beasts.org/powerbox.html" rel="nofollow">http://plash.beasts.org/powerbox.html</a>

I've been working on a little thing called Aether (it'll be ready for showing off by February, hopefully) which could help solve this problem. As part of its design, file choosers end up happening outside of an application. A few bells and whistles later and we have your idea!

</div>
</article></li>
 	<li id="comment-2008" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2008" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Ethan Anderson</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-01-21T20:51:03+00:00"> January 21, 2010 at 8:51 pm </time></div>
</footer>
<div class="comment-content">

&gt;Isn't Android neat? 😀

</div>
</article></li>
 	<li id="comment-2009" class="comment even thread-even depth-1"><article id="div-comment-2009" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Dylan McCall</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-01-22T05:15:26+00:00"> January 22, 2010 at 5:15 am </time></div>
</footer>
<div class="comment-content">

&gt;Oh, nifty! Thanks for pointing that out, Ethan. I didn't realize Android did this.

Just so you know, jQuigs, that's over here:
<a href="http://developer.android.com/guide/topics/data/data-storage.html" rel="nofollow">http://developer.android.com/guide/topics/data/data-storage.html</a>

</div>
</article></li>
 	<li id="comment-2010" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2010" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">gQuigs</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-01-22T05:29:07+00:00"> January 22, 2010 at 5:29 am </time></div>
</footer>
<div class="comment-content">

&gt;Dylan: Awesome! Looking forward to it.
Ethan: yes 🙂

</div>
</article></li>
</ul>
 </body></html>