<html><body><p>I'm considering a proposal to have 16.04 LTS be the last release of Ubuntu with 32 bit images to run on 32 bit only machines (on x86 aka Intel/AMD only - this has no bearing on ARM). You would still be able to run 32 bit applications on 64 bit Ubuntu.

Please answer my survey on how this would affect you or your organization.

<strong>Please only answer if you are running 32-bit (x86) Ubuntu! Thanks!</strong>

Form Closed.

<strong>Comments</strong>

 
</p><ul>
 	<li id="comment-5299" class="comment even thread-even depth-1 parent"><article id="div-comment-5299" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">dragonbite</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-21T16:33:33+00:00"> October 21, 2014 at 4:33 pm </time></div>
</footer>
<div class="comment-content">

Something like this is inevitable, but 32 bit is still helpful to have available.

Myself, I have a number of old, single-core desktops running Ubuntu Server that cannot handle 64 bits, but are able to work as servers just fine.

I also have a 64bit capable netbook I run 32bit Lubuntu on because of resources.

Maybe make it so that a minimal disk or server disk is available 32 bit for a little bit longer, after it is dropped for desktop-orientated systems. Those that need a desktop and 32bit can install minimum and then add whatever is needed for the circumstance.

</div>
</article>
<ol class="children">
 	<li id="comment-5300" class="comment odd alt depth-2 parent"><article id="div-comment-5300" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Rob van der Linde</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-21T18:21:24+00:00"> October 21, 2014 at 6:21 pm </time></div>
</footer>
<div class="comment-content">

Do I have to fill in the form for each machine I run, it looks like it as there is no field for how many machines?

I run 6 PC’s at home, all on 64 bit Kubuntu. My work machine is also 64 bit Kubuntu, and my 2 VM’s are also 64 bit. In fact, every VM I use at work is 64 bit as well.

That’s 9 machines on 64 bit and none on 32, I haven’t run 32 bit for years now.

Then I also have a couple of Beagleboards also running Trusty, but that’s ARM.

</div>
</article>
<ol class="children">
 	<li id="comment-5307" class="comment even depth-3 parent"><article id="div-comment-5307" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">bob</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T00:55:39+00:00"> October 22, 2014 at 12:55 am </time></div>
</footer>
<div class="comment-content">

Did you miss this: “Please only answer if you are running 32-bit (x86) Ubuntu!” ???

</div>
</article>
<ol class="children">
 	<li id="comment-5309" class="comment byuser comment-author-bryan bypostauthor odd alt depth-4"><article id="div-comment-5309" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T02:05:24+00:00"> October 22, 2014 at 2:05 am </time></div>
</footer>
<div class="comment-content">

To be fair, I added it because I was getting a lot of 64 bit users responding. Still it’s in the title…

</div>
</article></li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
 	<li id="comment-5308" class="comment even thread-odd thread-alt depth-1"><article id="div-comment-5308" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">bob</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T00:57:36+00:00"> October 22, 2014 at 12:57 am </time></div>
</footer>
<div class="comment-content">

Phoronix <a href="http://www.phoronix.com/forums/showthread.php?107903-Ubuntu-16-04-Might-Be-The-Distribution-s-Last-32-Bit-Release" rel="nofollow">http://www.phoronix.com/forums/showthread.php?107903-Ubuntu-16-04-Might-Be-The-Distribution-s-Last-32-Bit-Release</a>

</div>
</article></li>
 	<li id="comment-5310" class="comment odd alt thread-even depth-1 parent"><article id="div-comment-5310" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn"><a class="url" href="http://vasilisc.com" rel="external nofollow">vasilisc</a></b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T04:01:52+00:00"> October 22, 2014 at 4:01 am </time></div>
</footer>
<div class="comment-content">

My organization use Ubuntu Server LTS 32bits in virtual enviroment.

</div>
</article>
<ol class="children">
 	<li id="comment-5311" class="comment byuser comment-author-bryan bypostauthor even depth-2 parent"><article id="div-comment-5311" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T04:21:04+00:00"> October 22, 2014 at 4:21 am </time></div>
</footer>
<div class="comment-content">

Why? and what version of Ubuntu?

</div>
</article>
<ol class="children">
 	<li id="comment-5313" class="comment odd alt depth-3 parent"><article id="div-comment-5313" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn"><a class="url" href="http://vasilisc.com" rel="external nofollow">vasilisc</a></b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T05:01:16+00:00"> October 22, 2014 at 5:01 am </time></div>
</footer>
<div class="comment-content">

1) Virtual Machine with guest OS Ubuntu Server LTS 32bit less consumption RAM, right?
2) Ubuntu Server 14.04 LTS

</div>
</article>
<ol class="children">
 	<li id="comment-5317" class="comment byuser comment-author-bryan bypostauthor even depth-4"><article id="div-comment-5317" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T12:06:52+00:00"> October 22, 2014 at 12:06 pm </time></div>
</footer>
<div class="comment-content">

It is less RAM consumption, but usually in the 64 MB to 128 MB range. Obviously can be worse depending on the app you are running.

Generally the performance trade off makes it not worth it.

</div>
</article></li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
 	<li id="comment-5316" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-5316" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn"><a class="url" href="https://wiki.ubuntu.com/amjjawad" rel="external nofollow">Ali Linx</a></b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T09:23:38+00:00"> October 22, 2014 at 9:23 am </time></div>
</footer>
<div class="comment-content">

Hi, it is good idea to run such surveys and it is good point to bring on the table. Old machines should not go to trash unless these are 100% dead. I realized that even Lubuntu or other distributions won’t be helpful in the coming years and for that, I have created this project: <a href="http://torios.org/" rel="nofollow">http://torios.org/</a> which is still Alpha at the moment but we are moving forward with solid steps and Beta is just around the corner. Now, ToriOS is based on Ubuntu 12.04 LTS which is well-known to be better on old hardware than 14.04 LTS and for that reason, I insisted to base ToriOS on 12.04 and the team has agreed. Now, what could happen when 16.04 is out and just in case Canonical decided to end the 32bit support by that cycle? or perhaps even before that? 14.04 LTS could be the last one? who knows? not to worry, ToriOS will make sure that old machines will stay in service as long as possible. Only time can tell and prove that 🙂

Thanks!

</div>
</article></li>
 	<li id="comment-5320" class="comment even thread-even depth-1 parent"><article id="div-comment-5320" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Walter Lapchynski</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T15:32:14+00:00"> October 22, 2014 at 3:32 pm </time></div>
</footer>
<div class="comment-content">

My workplace uses 32 bit machines almost exclusively, using Ubuntu Server and FreeBSD for servers and Kubuntu for desktop. We have a mission that includes a commitment to being good environmental stewards. Our machines come from the local electronics recycling store. I admit we are a strange case, but why should we abandon our commitment to older machines when we officially support something committed to them (Lubuntu)?

That being said, reading Mark Shuttleworth’s wiki page recently helped me understand that Ubuntu is not the distro for every case. Limited scope is necessary to achieve intended goals.

Still, I love Ubuntu and it’s community but don’t want to contribute to landfills by buying new stuff just to keep using it.

On the other hand one of our staff has been looking for the excuse to go FreeBSD as a desktop (note we are a manufacturer of a mechanical product i.e. our staff is largely not computer savvy). Please don’t make those of us at the company that provide user support suffer that curse!

I guess the possibility exists to do community supported releases like we do for ppc, no?

Finally, do I really need to fill this out for every machine? There are 30-40 of them.

</div>
</article>
<ol class="children">
 	<li id="comment-5321" class="comment byuser comment-author-bryan bypostauthor odd alt depth-2 parent"><article id="div-comment-5321" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T15:45:43+00:00"> October 22, 2014 at 3:45 pm </time></div>
</footer>
<div class="comment-content">

&gt;I guess the possibility exists to do community supported releases like we do for ppc, no?

Members of the Lubuntu community have already expressed interest in making it community supported for Lubuntu. So the possibility definitely exists. Please do fill out the form as though that’s not going to happen though…

&gt;Finally, do I really need to fill this out for every machine? There are 30-40 of them.
Generally no, just one entry and say their are 35 of them. If there are substantial differences between them breaking them up could be useful..

One thing I have noticed is that there is a high rate of people thinking they have a 32-bit machine when they have on capable of 64 bit. The only way I can confirm that myself is be seeing the processor. Then having RAM is useful to, because 64 bit on 1 GB is not fun.

Of the last, let’s say 5, machines you got from the recycling how many were 64 bit capable?

</div>
</article>
<ol class="children">
 	<li id="comment-5323" class="comment even depth-3 parent"><article id="div-comment-5323" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Walter Lapchynski</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T17:28:21+00:00"> October 22, 2014 at 5:28 pm </time></div>
</footer>
<div class="comment-content">

&gt; One thing I have noticed is that there is a high rate of people thinking they have a 32-bit machine when they have on capable of 64 bit. The only way I can confirm that myself is be seeing the processor. Then having RAM is useful to, because 64 bit on 1 GB is not fun. Of the last, let’s say 5, machines you got from the recycling how many were 64 bit capable?

I understand your plight. I just wish there was a way to include info for multiple machines at a time. We actually have several machines that are of the same model and everything.

Most of the machines we have are HP dc7800 SFF (SKU#GC760AV), using an Intel Core 2 Duo T5470 (type 0, family 6, model 15, stepping 13). So it is actually 64 bit capable, since /proc/cpuinfo does include the “lm” flag.

We made the decision to go with 32 bit since we didn’t know what we’d end up with. I think that this may not be so relevant any more. That being said, how would we solve this? We would have to re-install every machine? You can’t “upgrade” to 64 bit can you?

</div>
</article>
<ol class="children">
 	<li id="comment-5324" class="comment byuser comment-author-bryan bypostauthor odd alt depth-4"><article id="div-comment-5324" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T18:23:33+00:00"> October 22, 2014 at 6:23 pm </time></div>
</footer>
<div class="comment-content">
<blockquote>We made the decision to go with 32 bit since we didn’t know what we’d end up with. I think that this may not be so relevant any more. That being said, how would we solve this? We would have to re-install every machine? You can’t “upgrade” to 64 bit can you?</blockquote>
You can reinstall in place (but backup first!) and choose the “Upgrade Ubuntu” option in the installer. I’ve moved machines from 32 to 64 bit using it, but it’s not heavily tested. One of the big things I’ve gotten from this survey to make an 32-&gt;64 supported upgrade path (even if it can’t be a dist-upgrade path).

</div>
</article></li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
 	<li id="comment-5325" class="comment even thread-odd thread-alt depth-1 parent"><article id="div-comment-5325" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">javier</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T18:43:56+00:00"> October 22, 2014 at 6:43 pm </time></div>
</footer>
<div class="comment-content">

I am using Ubuntu 10.04.4 LTS 32 bit installed with Wubi! Main OS: Vista Business 32bit

</div>
</article>
<ol class="children">
 	<li id="comment-5326" class="comment byuser comment-author-bryan bypostauthor odd alt depth-2"><article id="div-comment-5326" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T18:52:58+00:00"> October 22, 2014 at 6:52 pm </time></div>
</footer>
<div class="comment-content">

As a server? 10.04 for the destkop isn’t supported for 1.5+ years now.

</div>
</article></li>
</ol>
</li>
 	<li id="comment-5328" class="comment even thread-even depth-1 parent"><article id="div-comment-5328" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Walter Lapchynski</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T19:25:32+00:00"> October 22, 2014 at 7:25 pm </time></div>
</footer>
<div class="comment-content">

One other thought: are you dropping 32-bit support across all chips or is this only affecting Intel chips? Since Lubuntu is supporting PPC (primarily 32-bit), it would be a huge bummer if this affected PPC, too.

</div>
</article>
<ol class="children">
 	<li id="comment-5329" class="comment byuser comment-author-bryan bypostauthor odd alt depth-2"><article id="div-comment-5329" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-22T20:11:07+00:00"> October 22, 2014 at 8:11 pm </time></div>
</footer>
<div class="comment-content">

This survey/proposal isn’t touching on PPC.

</div>
</article></li>
</ol>
</li>
 	<li id="comment-5335" class="comment even thread-odd thread-alt depth-1"><article id="div-comment-5335" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">BGBgus</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-23T19:17:28+00:00"> October 23, 2014 at 7:17 pm </time></div>
</footer>
<div class="comment-content">

But, what would happen to the rest of tastes of Ubuntu? Distros like Xubuntu depends on “small” computers and give us a way to keep using our old but still working PC’s.

I admit, I would never use Unity on my Pentium, but i still like to keep my repositories updated. Will Xubuntu just disapear? I could look for another GNU’s distribution, but it’s still a lost.

</div>
</article></li>
 	<li id="comment-6368" class="comment odd alt thread-even depth-1"><article id="div-comment-6368" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">oldcomputerfan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-12-15T16:08:35+00:00"> December 15, 2014 at 4:08 pm </time></div>
</footer>
<div class="comment-content">

Hallo,
Ich würde es schade finden wenn die 32 BIT Versionen weg fallen.
Dann wird es viele auf Ubuntu- basierende Distributionen für ältere Computer nicht mehr geben.
Gruß

</div>
</article></li>
 	<li id="comment-6670" class="comment even thread-odd thread-alt depth-1 parent"><article id="div-comment-6670" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Aaron</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-19T02:34:58+00:00"> April 19, 2016 at 2:34 am </time></div>
</footer>
<div class="comment-content">

Hi…

I run Lubuntu 14.04 32 bit on both my laptop and desktop systems. For myself personally, although my laptop is a 64 bit system, I’ve purposely chosen the 32 bit version of Lubuntu because I’ve found that a couple Linux games (that are available in the repositories) don’t crash with segmentation fault errors as they did when I was running Ubuntu 10.04 64 bit. I’m inclined to think there are still bugs to iron out with respect to running 32 bit programs on 64 bit Linux operating systems.

As part of my work as a computer repair technician, I’ve also installed a 32 bit versions of Linux for a couple clients with older systems where (installing) Windows was not an option, including for financial reasons. This is where Linux fills an important role. The fact that there are 32 bit distributions available, free of charge, for older systems helps keep perfectly usable computers in the hands of those who cannot afford (or easily afford) to purchase a new(er) computer. And there are many people out there who are poor and in tight positions financially.

For this reason especially, I request (for all distributions within the Ubuntu family) that this decision be delayed until enough 32 bit computers have been recycled/disposed of to where those that are left are in the extreme minority.

Thank you for your time and consideration. 🙂

</div>
</article>
<ol class="children">
 	<li id="comment-6671" class="comment byuser comment-author-bryan bypostauthor odd alt depth-2 parent"><article id="div-comment-6671" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-19T14:37:42+00:00"> April 19, 2016 at 2:37 pm </time></div>
</footer>
<div class="comment-content">

That is the goal… determining how judge when minority is the right time is the hard part.

Please do test those games on 16.04 64-bit when you get a chance – feel free to report bugs here.

</div>
</article>
<ol class="children">
 	<li id="comment-6672" class="comment even depth-3 parent"><article id="div-comment-6672" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Aaron</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-20T17:27:09+00:00"> April 20, 2016 at 5:27 pm </time></div>
</footer>
<div class="comment-content">

Hi Bryan…

I did issue a bug report a few years ago on one of the bugs but it was never acted on. 🙁

<a href="https://bugs.launchpad.net/ubuntu/+source/tecnoballz/+bug/980091" rel="nofollow">https://bugs.launchpad.net/ubuntu/+source/tecnoballz/+bug/980091</a>

</div>
</article>
<ol class="children">
 	<li id="comment-6673" class="comment byuser comment-author-bryan bypostauthor odd alt depth-4 parent"><article id="div-comment-6673" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-20T17:51:42+00:00"> April 20, 2016 at 5:51 pm </time></div>
</footer>
<div class="comment-content">

Yea, we generally don’t have enough resources to review every bug people report, sorry. If you can reproduce on Ubuntu 16.04, please report a new bug.

Feel free to ping me here or on LP, if you find it still occurs.

</div>
</article>
<ol class="children">
 	<li id="comment-6675" class="comment even depth-5"><article id="div-comment-6675" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Aaron</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-21T07:43:45+00:00"> April 21, 2016 at 7:43 am </time></div>
</footer>
<div class="comment-content">

Thanks! 🙂

</div>
</article></li>
 	<li id="comment-6676" class="comment odd alt depth-5"><article id="div-comment-6676" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Aaron</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-21T07:49:45+00:00"> April 21, 2016 at 7:49 am </time></div>
</footer>
<div class="comment-content">

Hi Bryan…

If I have an occasion to try 16.04, I might do that. 🙂

</div>
</article></li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
</ol>
</li>
 	<li id="comment-6674" class="comment even thread-even depth-1 parent"><article id="div-comment-6674" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">witchyseattle</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-21T07:18:36+00:00"> April 21, 2016 at 7:18 am </time></div>
</footer>
<div class="comment-content">

I myself am on a 32 bit and it would be devastating if I no longer could download Ubuntu. Isn’t the whole point is inclusion ? I only have 2 gig of RAM and cannot use a 64 on this laptop. So much for Ubuntu, they should change their name to sell out, because they are leaving a whole bunch of people behind, just because they are not running 64 bit and that is not fair !

</div>
</article>
<ol class="children">
 	<li id="comment-6677" class="comment byuser comment-author-bryan bypostauthor odd alt depth-2"><article id="div-comment-6677" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-04-22T13:38:23+00:00"> April 22, 2016 at 1:38 pm </time></div>
</footer>
<div class="comment-content">

First of all, no decision has been made – in fact 32 bit is fully supported for 16.04 LTS. Secondly, Ubuntu is available for free and it costs money for each architecture supported.

Provide cat /proc/cpuinfo and we can double check if you definitely can’t run 64 bit.

</div>
</article></li>
</ol>
</li>
 	<li id="comment-6684" class="comment even thread-odd thread-alt depth-1"><article id="div-comment-6684" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Timothy D Lynch</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2016-07-20T23:36:15+00:00"> July 20, 2016 at 11:36 pm </time></div>
</footer>
<div class="comment-content">

I still use 32 bit on about 5 units and would most likely switch to another distro with 32 bit support to have the uniform. This is the same reason I waited so long to change from 10.04 Ubuntu and will most likely go from 12.04 and do 2 upgrades to 16.04 to use Mate. Old Hardware. I guess I’m cheap and if the hardware is still working I keep using it. The getting it on the cheap is why I quite using Window even though at one time I owned a business supporting it.

</div>
</article></li>
 	<li id="comment-6724" class="comment odd alt thread-even depth-1"><article id="div-comment-6724" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Weasel</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2017-02-18T20:28:00+00:00"> February 18, 2017 at 8:28 pm </time></div>
</footer>
<div class="comment-content">

Well I’m late to the party I suppose. I find it absurd to drop an architecture like x86 (32) when you support PPC, but that doesn’t matter.

For most VMs, 32-bit is much better as it uses less resources. Not just RAM, but disk as well. Especially if you want to run 32-bit apps within the VM, which would require multilib, making the disk space difference that much more than on a pure 32-bit VM. Anyway, running multiple “slim” VMs in parallel tends to make it that much more obvious.

Look, if you don’t want to support 32-bit as in “test it on every ‘ancient’ machine” then that’s still not so bad as dropping it. The problem isn’t only lack of technical support here, but as you see, LACK OF DOWNLOAD. You can drop “technical support” without dropping the download ISO file for those interested, like to run it in a VM — why should we care of real hardware anyway? Still, we need an iso. To me it just sounds like an excuse to be honest.

But you take it away from everyone by dropping the download image. That’s why it needs to be preserved as an OPTION, even if not on main download page. Some things need to be “preserved”. Either way bandwidth wouldn’t be a problem since if it’s rarely downloaded then it doesn’t matter.

“Popularity” isn’t the issue. It’s just having it *available* for anyone wishing to use it (e.g. for a VM).

As a sidenote, people on the internet keep throwing around the word “use VM for old stuff” but how to use VM when we’re not provided the OS anymore? wtf.

</div>
</article></li>
</ul>
 </body></html>
