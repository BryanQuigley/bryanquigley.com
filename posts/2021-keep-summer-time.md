<!--
.. title: Let's keep time like it is in the summer
.. slug: keep-summer-time
.. date: 2021-05-14 19:45:00+00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

**If you are in the USA - Please use my new site [KeepSummerTime.com](https://keepsummertime.com/) to write to your congresspeople asking for summer time all year long.**

The USA has an active bill in congress to keep us from changing the clocks and stay on time like it is in the summer year round (also called permanent DST). Changing the clocks has not been shown to have [substantial benefits](https://keepsummertime.com/about/#what-about) and the harms have been [well documented](https://en.wikipedia.org/wiki/Sunshine_Protection_Act). 

For global communities - like FLOSS -

 * It makes it **that** much harder to schedule across the world. 
 * The majority of the world [does not](https://en.wikipedia.org/wiki/Daylight_saving_time_by_country) do clock switching. It's generally EU/US specific.

**If you are in the USA - Please use my new site [KeepSummerTime.com](https://keepsummertime.com/) to write to your congresspeople asking for summer time all year long.**

If you want to help out 

 * the site is all available on [Github](https://github.com/BryanQuigley/keepsummertime.com/) although the actual contact congress bit is from ActionNetwork. 
 * I'd be very happy to make this site global in nature for all of us stuck with unstable time.  Please get in touch!