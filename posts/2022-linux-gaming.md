<!--
.. title: Linux Gaming in 2022
.. slug: linux-gaming-2022
.. date: 2022-02-27 09:07:45+00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

# Quick thoughts

I'd bet the Steam Deck (and other changes) will have the following impacts on Linux overall by the end of 2022.

1. The majority of Linux users will run Wayland over X11.
2. Valve’s Steamdeck is going to double the number of Linux gamers per Valve's Hardware Survey.
3. Flatpak/Flathub will ride the Deck wave - usage will double.
4. Distros will ride the Deck wave - gaming usage will increase by about 20% (1% -> 1.20%).


# 1 Majority Wayland

<img src="/images/2022/Wayland_logo.svg" width="100">

Currently we are at less than 10% running wayland per [Firefox telemetry stats on Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Firefox-Wayland-X11-Stats) but there are a lot of movers, namely:

1. Ubuntu 22.04 LTS will be the first Ubuntu LTS release defaulting to Wayland.
1. Nvidia drivers explicitly improving Wayland support.
1. Although this is still a [big list](https://community.kde.org/Plasma/Wayland_Showstoppers), KDE wayland support has been getting a lot of improvements recently and it's the default on some installs.
1. Steam Deck will be using [Wayland](https://partner.steamgames.com/doc/steamdeck/faq).

# 2 Double Steam Linux users

<img src="/images/2022/logo_steam.svg" width="200">

Right now for January 2022 1.06% of Steam users are running Linux. I estimate about 340-460k Steam Linux users (Valve published Flatpak installs for November at about 5%. There are approximately 17000-23000 users for each update).

We don't have current numbers for Steam deck reservations, but near launch it [was > 100k](https://twitter.com/thexpaw/status/1416095435316473858?lang=en). Selling 300k-500k seems quite within the realm of possibilities. I would also not be surprised if they sold more.

# 3 Flathub usage doubles

<a href='https://flathub.org/apps'><img width='240' alt='Download on Flathub not a link' src='/images/2022/flathub-badge-en.svg'/></a>

Flatpak is the easiest way to install non-Steam software on a Steam deck - "Yes. You'll be able to install external apps via Flatpak or other software without going into developer mode" - [Steam Deck FAQ](https://partner.steamgames.com/doc/steamdeck/faq)

The key items I see at first would be [MineCraft](https://flathub.org/apps/details/com.mojang.Minecraft) and the large collection of [gaming emulators](https://flathub.org/apps/search/emulator). It would also be the obvious choice if another studio wanted to bring a game to the Deck.

Valve has avoided picking sides regarding Flatpak vs Snap vs AppImage so far. They still offer a deb from their own download page. Given Steam's user base just making the Flatpak the default would likely more than double Flatpak usage.

There are more wildcards here: 

 * How actually easy will flatpak be?
 * How hard are other options - snaps (requires dev mode?), AppImage (seems like it might work fine), etc?

# 4 Distros ride the wave

I'm expecting at least 20% increase on the Steam Hardware Survey (so 1% to 1.20%) not including Steam Deck. Right now, the Steam Linux usage is less than half what you get from other sources. That could mean multiple things:

 * Linux is less likely to be used by gamers
 * Linux users prefer playing awesome open source games (or otherwise don't use Steam)
 * Users switch to game on other platforms. Many users might take a fresh look at Steam on their existing Linux boxes with all the press from the Steam Deck.

All are likely true to some extent. 

Linux distros have many options to further ride the wave:

 * Encourage Linux consumer focused pre-installs with similar AMD chips to what's in the Steam Deck (I know many have asked for more AMD preinstalls for a long while)
 * Enable Flathub/flatpak by default for easier finding apps (Steam itself is not discoverable on Ubuntu unless you enable flatpak. If flatpak takes off more, this will be quite essential)
 * Gaming on Linux has been discussed more on mainstream tech shows than at any point in my memory. This is a marketing opportunity to not pass up.
 * Help support Game studios with Linux porting and compatibility. (or show other stores they can come!)

# Comments

 Do you think these will all come to pass?  Was I way off? Add a comment via [Gitlab](https://gitlab.com/BryanQuigley/bryanquigley.com/-/blob/master/posts/2022-linux-gaming.md)