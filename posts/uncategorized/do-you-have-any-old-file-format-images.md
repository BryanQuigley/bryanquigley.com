<html><body><p>I'm specifically looking for:
OS/2 Metafile (.met)
PICT (Mac's precursor to PDF) https://en.wikipedia.org/wiki/PICT

Also useful might be:
PCD - Kodak Photo CD
RAS - Sun Raster Image

I'm trying to evaluate if LibreOffice should keep support for them (specifically if the support is good). Unfortunately I can only generate the images using LibreOffice (or sister projects) which doesn't really provide a great test.

Please either:
* Provide a link in a comment below
* Email me B @ (If emailed, please mention if I can share the image publicly)

If I find the support works great I'd try to integrate a few of them into LO tests so we make sure they don't regress.

Feedback:
</p><div class="comment-content">

I know that the ffmpeg project has a huge set of sample images:
<a href="https://samples.ffmpeg.org/image-samples/" rel="nofollow">https://samples.ffmpeg.org/image-samples/</a>

Especially:
<a href="https://samples.ffmpeg.org/image-samples/sunrast/" rel="nofollow">https://samples.ffmpeg.org/image-samples/sunrast/</a>

Maybe this is of help to you,

Take a look at the File Formats wiki, they may have links to samples:

http://fileformats.archiveteam.org/

Please contribute to the wiki if you can.

</div>
Thank you!  [Update, files are now part of LibreOffice's test server]</body></html>