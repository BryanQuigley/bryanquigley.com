<html><body><a href="/wp-content/uploads/2014/04/memoryusagegraph.png"><img class="alignnone size-full wp-image-2029" src="/wp-content/uploads/2014/04/memoryusagegraph.png" alt="memoryusagegraph" width="605" height="340"></a>

I did a comparison when 14.04 was first released on the memory usage of different Ubuntu flavors.   Some takeaways:
<ul>
	<li>Lubuntu has <a href="https://en.wikipedia.org/wiki/Zram">zRam</a> (automatically compress memory to save space) enabled by default making it hands down the most usable version with low-memory.  It's the only flavor to have it enabled on the LiveCD.</li>
	<li>The real cost of 64 vs 32 bit is usually only 128 MB.</li>
	<li>Lubuntu (32-bit) still boots with just 160 MB of RAM!</li>
	<li>The Ubuntu kernel can't boot with only 128 MB of RAM.</li>
</ul>
All testing was done in virtual machines (Virtualbox) and obviously with different hardware you're results will vary.   You can infer some of my methodology from the notes below.   This was done months ago and I don't remember all of the details.  The results may have changed with software updates, especially to Firefox.

<a href="http://bryanquigley.com/wp-content/uploads/2014/04/memoryusage.ods">S</a><a href="http://bryanquigley.com/wp-content/uploads/2014/04/memoryusage.ods">preadsheet</a>

Raw testing <a href="http://bryanquigley.com/wp-content/uploads/2014/04/MemoryUsage.txt">notes </a></body></html>