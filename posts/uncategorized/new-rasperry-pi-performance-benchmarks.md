<html><body><p>Lazy follow up to my <a title="Rasberry Pi vs old Dell, P4" href="http://bryanquigley.com/uncategorized/rasberry-pi-vs-old-dell-p4">previous post </a>on the Raspberry Pi.  Here are some more recent benchmarks using a more up-to-date image:  <a href="http://openbenchmarking.org/result/1302242-BY-1205272AR49">http://openbenchmarking.org/result/1302242-BY-1205272AR49</a>

Take aways:  Performance has improved greatly.   Still get's crushed by a P4, but performance per watt is now clearly in the Pi's favour.

Some of the tests failed because I don't have a monitor hooked up to me Pi.  (Like GTK perf).  With the Wayland changes, I guess I might have to review this again with one..</p></body></html>