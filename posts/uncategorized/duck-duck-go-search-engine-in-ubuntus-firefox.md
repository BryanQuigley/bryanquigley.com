<html><body><p>If you haven't tried searching with Duck Duck Go, give it a try at <a title="https://duckduckgo.com/" href="https://duckduckgo.com/">duckduckgo.com</a>

Learn more about how it protects your privacy and doesn't track you.. <a title="http://donttrack.us/" href="http://donttrack.us/">donttrack.us</a>

Learn more about how it gives back to free and open source software. <a href="http://web.archive.org/web/20140921192852/http://www.gabrielweinberg.com/blog/2010/11/help-me-start-a-foss-tithing-movement.html">http://www.gabrielweinberg.com/blog/2010/11/help-me-start-a-foss-tithing-movement.html</a>

I previously posted about trying to add Duck Duck Go <a href="http://bryanquigley.com/uncategorized/duck-duck-go-in-firefox">directly to Firefox</a> (that has stalled).   My proposal for Ubuntu is a bit different, I think it makes sense for us to replace Bing with Duck Duck Go.  Every search helps the company behind <a href="https://bugs.launchpad.net/ubuntu/+bug/1">bug #1</a>, in fact I would argue that including Bing helps them keep up their market-share.

If you like the idea of Duck Duco Go being added to Ubuntu's Firefox search box above, please show your support! <a href="http://brainstorm.ubuntu.com/idea/28078/">http://brainstorm.ubuntu.com/idea/28078/</a>

Also, and just as important as the above, Duck Duck Go provides really awesome results!  Try a few... <a href="https://duck.co/topic/wow-queries-that-showcase-ddg">https://duck.co/topic/wow-queries-that-showcase-ddg</a></p></body></html>