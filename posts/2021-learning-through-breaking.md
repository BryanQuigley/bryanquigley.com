<!--
.. title: Learning through breaking
.. slug: learning-through-breaking
.. date: 2021-03-03 23:40:45+00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I run [Steam in a flatpak](https://flathub.org/apps/details/com.valvesoftware.Steam) for convenience and confinment reasons. One day my Steam install failed with

![32 bit libarires not installed](/images/2021-learning-through-breaking-steam.png)

 My first instinct is to check to make sure libc6:i386 is actually installed - it is. Then I check to see if there are flatpak updates, but with the 32-bit libraries I find more errors:

```bash
        ID                                                  Branch        Op        Remote         Download
 1. [✗] org.freedesktop.Platform.GL32.nvidia-460-39         1.4           i         flathub        178.7 MB / 178.7 MB

Error: While trying to apply extra data: apply_extra script failed, exit status 40704
error: Failed to install org.freedesktop.Platform.GL32.nvidia-460-39: While trying to apply extra data: apply_extra script failed, exit status 40704
```

Journal log
```bash
Feb 26 08:18:24 desktop polkitd(authority=local)[641]: Registered Authentication Agent for unix-process:3535:65589 (system bus name :1.75 [flatpak install org.freedesktop.Platform.GL32.nvidia-460-39], object path /org/freedesktop/PolicyKit1/AuthenticationAgent, locale en_US.UTF-8)
Feb 26 08:18:26 desktop flatpak[3535]: libostree pull from 'flathub' for runtime/org.freedesktop.Platform.GL32.nvidia-460-39/x86_64/1.4 complete
                                       security: GPG: summary+commit 
                                       security: SIGN: disabled http: TLS
                                       delta: parts: 1 loose: 3
                                       transfer: secs: 0 size: 349.8 kB


Feb 26 08:18:54 desktop flatpak[3535]: system: Pulled runtime/org.freedesktop.Platform.GL32.nvidia-460-39/x86_64/1.4 from flathub
Feb 26 08:18:55 desktop audit[3583]: SECCOMP auid=1000 uid=0 gid=0 ses=2 subj==unconfined pid=3583 comm="apply_extra" exe="/app/bin/apply_extra" sig=31 arch=40000003 syscall=122 compat=1 ip=0x80a933d code=0x0
```

This is where I remember that I've been testing a lot of systemd confinement changes (including limiting SystemCalls) and one of the services I modified was gpg-agent. However, reverting that change doesn't help but I'm getting closer. (Aside: Great time to guess what config change I made that caused the errors..)

I then run:
```bash
sudo flatpak repair
```
to verify all the files in flatpak but nothing needed fixing.

I then ran:
```bash
$ sudo dpkg -V
...
/etc/systemd/system.conf
...
```

Oh, shoot I did setup
```bash
SystemCallArchitectures=native
```
This is saying I only want native syscalls to be run, but why is it applying to an application! I would have thought it just applied to services or other things systemd runs.

Sure enough disabling that option fixes it, Steam works, and the 32-bit NVidia via Flatpak install too.

## But.. why?

Flatpak runs apps in a systemd scope (if available).

```bash
$ systemctl status --user app-flatpak-com.valvesoftware.Steam-6702.scope 
● app-flatpak-com.valvesoftware.Steam-6702.scope
     Loaded: loaded (/run/user/1000/systemd/transient/app-flatpak-com.valvesoftware.Steam-6702.scope; transient)
  Transient: yes
     Active: active (running) since Wed 2021-03-03 12:16:23 PST; 1min 2s ago
      Tasks: 113 (limit: 38415)
     Memory: 352.0M
        CPU: 16.066s
     CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/app-flatpak-com.valvesoftware.Steam-6702.scope
             ├─6702 bwrap --args 41 /app/bin/steam-wrapper
             ├─6706 bwrap --args 4But what does 1 xdg-dbus-proxy --args=43
             ├─6707 xdg-dbus-proxy --args=43
             ├─6711 bwrap --args 41 /app/bin/steam-wrapper
             ├─6713 bash /home/bryan/.local/share/Steam/steam.sh
            ....etc
```

I want to explore inside this scope more and I stumble upon [some Sandbox docs](https://github.com/flatpak/flatpak/wiki/Sandbox), but using flatpak run just creates it's own scope:

```bash
$ systemctl status --user app-flatpak-com.valvesoftware.Steam-7616.scope 
● app-flatpak-com.valvesoftware.Steam-7616.scope
     Loaded: loaded (/run/user/1000/systemd/transient/app-flatpak-com.valvesoftware.Steam-7616.scope; transient)
  Transient: yes
     Active: active (running) since Wed 2021-03-03 12:20:21 PST; 33s ago
      Tasks: 6 (limit: 38415)
     Memory: 2.8M
        CPU: 61ms
     CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/app-flatpak-com.valvesoftware.Steam-7616.scope
             ├─7616 bwrap --args 42 bash
             ├─7620 bwrap --args 42 xdg-dbus-proxy --args=44
             ├─7621 xdg-dbus-proxy --args=44
             ├─7624 bwrap --args 42 bash
             └─7626 bash
```

But this is an awesome way to see what the Flatpak actually has access to (and the package icon is just such a nice touch)

```bash
$ flatpak run --command=bash com.valvesoftware.Steam 
[📦 com.valvesoftware.Steam ~]$ ls
Music  Pictures  cache	config	data
[📦 com.valvesoftware.Steam ~]$ pwd
/home/bryan
```

I totally forgot that Steam has a built-in music player. Let's turn that off.

flatpak permissions-show or list doesn't seem to do anything.  

flatpak info --show-permissions com.valvesoftware.Steam is the right answer ([thanks!](https://unix.stackexchange.com/questions/476759/how-to-list-permissions-of-flatpak-applications))

```bash
filesystems=xdg-run/app/com.discordapp.Discord:create;xdg-pictures:ro;xdg-music:ro;
persistent=.;
```

I then decide to just install [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal) to review those and end up disabling all the default file permissions.


```bash
$ flatpak run --command=bash com.valvesoftware.Steam 
[📦 com.valvesoftware.Steam ~]$ ls
Music  Pictures  cache	config	data
```

Hmm.. Did I do something wrong?

```bash
$ ls Music/ Pictures/
Music/:

Pictures/:
```

Nope, those directories are now empty.  Previosly they were my actual music and pictures. Better confinement and a better understanding of how it works. Nice!

Have a comment or did I make a mistake?  Add it via [Gitlab](https://gitlab.com/BryanQuigley/bryanquigley.com/-/blob/master/posts/2021-learning-through-breaking.md).