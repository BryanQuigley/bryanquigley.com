<html><body><p>*Corrected mistake, thanks Martin.

I have previously posted about memory usage on <a title="32 vs 64 Bit – Memory" href="http://bryanquigley.com/for-beginners/32-vs-64-bit-memory">7.10</a>.

All tests were done with an Ubuntu LiveCD on the same machine for both 7.10 and 8.04.1. A network card may have changed between 7.10 and 8.04.1. Applications used were Firefox, OpenOffice.org Writer, Rhythmbox, Totem and Gimp. All values copied out of Gnome System Monitor.

Comparing Ubuntu 8.04.1 64 to 32

<style type="text/css"><!--
@page { size: 8.5in 11in; margin: 0.79in }   TD P { margin-bottom: 0in }   P { margin-bottom: 0.08in }
--></style>
</p><table width="100%" border="1" cellspacing="0" cellpadding="4"><colgroup> <col width="51"> <col width="51"> <col width="51"> <col width="102"></colgroup>
<tbody>
<tr valign="top">
<td width="20%" height="12"></td>
<td width="20%">32 bit</td>
<td width="20%">64 bit</td>
<td width="40%">Difference</td>
</tr>
<tr valign="top">
<td width="20%">Initial Boot</td>
<td width="20%">154.1</td>
<td width="20%">237.6</td>
<td width="40%">83.5</td>
</tr>
<tr valign="top">
<td width="20%">With Apps Open</td>
<td width="20%">262.7</td>
<td width="20%">395.7</td>
<td width="40%">133</td>
</tr>
</tbody>
</table>
I used the <a href="http://www.phoronix-test-suite.com/">Phoronix Test Suite</a> to attempt to get an idea of how ram access is changed from 32 bit to 64 bit. I was hoping to find a benefit for using 64 bit. I performed a very small test and did not find the benefit (in fact 64 bit was slighly slower). Results below:

Note: This was a very very small test.

Comparing 7.10 to 8.04.1, Initial Boot Only
<table width="100%" border="1" cellspacing="0" cellpadding="4">
<tbody>
<tr valign="top">
<td width="20%" height="12"></td>
<td width="20%">7.10</td>
<td width="20%">8.04.1</td>
<td width="40%">Difference</td>
</tr>
<tr valign="top">
<td width="20%">32 bit</td>
<td width="20%">146.0</td>
<td width="20%">154.1</td>
<td width="40%">+8.1</td>
</tr>
<tr valign="top">
<td width="20%">64 bit</td>
<td width="20%">212.9</td>
<td width="20%">237.6</td>
<td width="40%">+24.7</td>
</tr>
</tbody>
</table></body></html>
