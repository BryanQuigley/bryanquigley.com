<html><body><h3>Bye Tiny</h3>
Some recent hacking attempts at my site had convinced me to reduce the number of logins I had to protect on my personal site.   That's what motivated a move from the -still- awesome Tiny Tiny RSS that I've been using since Google Reader ended.   I only follow 13 sites and maintaining my own install simply doesn't make sense.

* None of the hacking attempts appeared to be targeting Tiny Tiny RSS ~ but then again I'm not sure if I would have noticed if they were.
<h3>Enter NewsBlur</h3>
My favorite site for finding <a href="https://alternativeto.net">alternatives to</a> software quickly settled on a few obvious choices.  Then I noticed that one of them was both <strong>Open Source</strong> and <strong>Hosted </strong>on their own servers with a <strong>freemium</strong> model.

<a href="https://www.newsblur.com/">It was NewsBlur</a>

I decided to try it out and haven't looked back.  The interface is certainly different than Tiny (and after 3 years I was very used to Tiny ) but I haven't really thought about it after the first week.   The only item I found a bit difficult to use was arranging folders ~ I'd really prefer drag and drop.   I only needed to do it once so not a big deal.

The free account has some limitations such as a limit to the number of feeds (64), limit to how fast they update, and no ability to save stories.   The premium account is only <strong>$24</strong> a year which seems very reasonable if you want to support this service or need those features.  As of this writing there were about 5800 premium and about 5800 standard users, which seems like a healthy ratio.

Some security notes: the site get's an <strong>A</strong> on  <a href="https://www.ssllabs.com/">SSLLabs.com</a> but they do have HSTS turned explicitly off.   I'm guessing they can't enable HSTS because they need to serve pictures directly off of other websites that are HTTP only.

NewsBlur's code is on <a href="https://github.com/samuelclay/NewsBlur">Github</a> including how to setup your own NewsBlur instance (it's designed to run on 3 separate servers) or for testing/development.   I found it particularly nice that the guide the site operator will check if NewsBlur goes down is public.  Now, that's transparency!

They have a bunch of other advanced features (still in free version) that I haven't even tried yet, such as:
<ul>
 	<li>finding other stories you would be interested (Launch Intel)</li>
 	<li>subscribing to email newsletters to view in the feed</li>
 	<li>Apps for Android, iPhone and suggested apps for many other OSes</li>
 	<li>Global sharing on NewsBlur</li>
 	<li>Your own personal (public in free version) blurblog to share stories and your comments on them</li>
</ul>
Give <a href="https://www.newsblur.com">NewsBlur</a> a try today.  Let me know if you like it!

I'd love to see more of this nice combination of hosted web service (with paid &amp; freemium version) and open source project.  Do you have a favorite project that follows this model?   Two others that I know of are <a href="https://www.odoo.com">Odoo</a> and <a href="https://www.draw.io/">draw.io.</a>

Comments:
<strong>Mihai</strong>

NewsBlur is awesome. I have been using it since the demise of Google Reader with no issues and almost no downtime. It has been getting better and better, especially the Android app.

 

<strong>teh 1</strong>

I tried NewsBlur a blue moon ago. How it may’ve changed I don’t know, but I’ve kept steady with CommaFeed (with a few hiccups in between with service availability in the first few years and loss of all starred items) since the demise of Google Reader. It’s open-source too: <a href="https://github.com/Athou/commafeed" rel="nofollow">https://github.com/Athou/commafeed</a>

<a href="https://www.commafeed.com/" rel="nofollow">https://www.commafeed.com/</a></body></html>