<html><body><p>&gt;<span style="font-style: italic;">How the Free versus Proprietary Software conflict frames our culture and ultimately government.</span>

This is the first large political science paper I've written (20 Pages). I do plan on releasing the final copy under CC licensing, or maybe make it a wiki page.

It is currently available as <a href="http://bryanquigley.com/wp-content/uploads/2008/06/PoliSciFinalPaper.html">HTML</a>, <a href="http://bryanquigley.com/wp-content/uploads/2008/06/PoliSciFinalPaper.pdf">PDF</a>, and <a href="http://bryanquigley.com/wp-content/uploads/2008/06/PoliSciFinalPaper.odt">ODT</a> if you would like to try to read it.

Please criticize, suggest, comment, etc. I am looking for feedback. Thanks.</p></body></html>