<!--
.. title: What packages are really required for Debian?
.. slug: debian-requirements
.. date: 2021-03-18 07:16:00+00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I used 2 of the variants supported by [mmdebstrap](https://gitlab.mister-muffin.de/josch/mmdebstrap/) to illustrate the different small build options.

Thanks to [Dan at EndlessOS](https://community.endlessos.com/u/dan) for showing me the much easier way:
```
$ grep-aptavail -n -s Package -F Essential yes
$ grep-aptavail -n -s Package -F Priority required
$ grep-aptavail -n -s Package -F Priority important
```

Essential
================
Uncompressed tarball size 94M

For when you don't even want to have apt. 


```bash
base-files
base-passwd
bash
bsdutils
coreutils
dash
debconf
debianutils
diffutils
dpkg
findutils
gcc-10-base:amd64
grep188M
init-system-helpers
libacl1:amd64
libattr1:amd64
libaudit-common
libaudit1:amd64
libblkid1:amd64
libbz2-1.0:amd64
libc-bin
libc6:amd64
libcap-ng0:amd64
libcom-err2:amd64
libcrypt1:amd64
libdb5.3:amd64
libdebconfclient0:amd64
libgcc-s1:amd64
libgcrypt20:amd64
libgmp10:amd64
libgpg-error0:amd64
libgssapi-krb5-2:amd64
libk5crypto3:amd64
libkeyutils1:amd64
libkrb5-3:amd64
libkrb5support0:amd64
liblz4-1:amd64
liblzma5:amd64
libmount1:amd64
libnsl2:amd64
libpam-modules:amd64
libpam-modules-bin
libpam-runtime
libpam0g:amd64
libpcre2-8-0:amd64
libpcre3:amd64
libselinux1:amd64
libsmartcols1:amd64
libssl1.1:amd64
libsystemd0:amd64
libtinfo6:amd64
libtirpc-common
libtirpc3:amd64
libudev1:amd64
libuuid1:amd64debian-requirements.md
zlib1g:amd64
```

Added in minbase
================
Uncompressed tarball size 123M

```bash
adduser
apt
debian-archive-keyring
e2fsprogs
gcc-9-base:amd64
gpgv
libapt-pkg6.0:amd64
libext2fs2:amd64
libffi7:amd64
libgnutls30:amd64
libhogweed6:amd64
libidn2-0:amd64
libnettle8:amd64
libp11-kit0:amd64
libseccomp2:amd64
libsemanage-common
libsemanage1:amd64Added in minbase
libxxhash0:amd64
logsave
mount
passwd
tzdata
```

Added in default variant
================
Uncompressed tarball size 188M

Theoretically all Priority: Important packages.

This is where items start to get a bit redundant IMHO. Mostly because I prefer the 
built-in systemd options as opposed to ifupdown, rsyslog/logrotate and cron.

```bash
apt-utils
cpio
cron
debconf-i18n
dmidecode
dmsetup
fdisk
ifupdown
init
iproute2
iputils-ping
isc-dhcp-client
isc-dhcp-common
kmod
less
libapparmor1:amd64
libargon2-1:amd64
libbpf0:amd64
libbsd0:amd64
libcap2:amd64
libcap2-bin
libcryptsetup12:amd64
libdevmapper1.02.1:amd64
libdns-export1110
libedit2:amd64
libelf1:amd64
libestr0:amd64
libfastjson4:amd64
libfdisk1:amd64
libip4tc2:amd64
libisc-export1105:amd64
libjansson4:amd64
libjson-c5:amd64
libkmod2:amd64
liblocale-gettext-perl
liblognorm5:amd64
libmd0:amd64
libmnl0:amd64
libncurses6:amd64
libncursesw6:amd64
libnewt0.52:amd64
libnftables1:amd64
libnftnl11:amd64
libpopt0:amd64
libprocps8:amd64
libreadline8:amd64
libslang2:amd64
libtext-charwidth-perl
libtext-iconv-perl
libtext-wrapi18n-perl
libxtables12:amd64
logrotate
nano
netbase
nftables
procps
readline-common
rsyslog
sensible-utils
systemd
systemd-sysv
systemd-timesyncd
tasksel
tasksel-data
udev
vim-common
vim-tiny
whiptail
xxd
```