<!--
.. title: Why hasn't snap or flatpak won yet?
.. slug: snap-flatpak-who-wins
.. date: 2021-06-01 20:00:00+00:00
.. tags: 
.. category: mindshare
.. link: 
.. description: 
.. type: text
-->

Where win means becomes *the* universal way to get apps on Linux.

In short, I don't think either current iteration will. But why?

I started writing this a while ago, but [Disabling snap Autorefresh](https://popey.com/blog/2021/05/disabling-snap-autorefresh/) reminded me to finish it. I also do not mean this as a "hit piece" against my former employer.

Here is a quick status of where we are:

| Use case                  | Snaps  | Flatpak |
|--------------             |--------|---------|
| Desktop app               |  ☑️    |   ☑️    |
| Service/Server app        |  ☑️    |   🚫    |
| Embedded                  |  ☑️    |   🚫    |
| Command Line apps         |  ☑️    |   🚫    |
| Full independence option  |  🚫    |   ☑️    |
| Build a complete desktop  |  🚫    |   ☑️    |
| Controlling updates       |  🚫    |   ☑️    |

# Desktop apps
Both Flatpaks and Snaps are pretty good at desktop apps.  They share [some bits](https://github.com/flatpak/xdg-desktop-portal) and have some differences. Flatpak might have a slight edge because it's focused only on Desktop apps, but for the most part it's a wash.

# Service/Server / Embedded / Command Line apps
Flatpak doesn't target these at all. Full stop.

Snap wins these without competition from Flatpak but this does show a security difference.  sudo snap install xyz will just install it - it won't ask you if you think it's a service, desktop app or some combination (or prompt you for permissions like Flatpak does).

With Embedded using Ubuntu Core it requires strict confinement which is a plus (Which you read correctly, means "something less" confinement everywhere else). 

Aside: As Fedora SilverBlue and Endless OS both only let you install Flatpaks, they also come with the container based [Toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/) to make it possible to run other apps. 

# Full independence option / Build a complete desktop

**Snaps**

You can not go and (re)build your own distro and use upstream snapd. 

Snaps are generally running from one LTS "core" behind what you might expect from your Ubuntu desktop version. For example: core18 is installed by default on Ubuntu 21.04. The embedded Ubuntu Core option is the only one that is using just one version of Ubuntu core code..

**Flatpak**

With Flatpak you can choose to use one of many public bases like the Freedesktop platform or Gnome platform. You can also build your own Platform like [Fedora Silverblue](https://silverblue.fedoraproject.org/) does. All of the default flatpak that Silverblue comes with are derived from the "regular" Fedora of the same version. You can of course add other sources too. Example: The Gnome Calculator from Silverblue is built from the Fedora RPMs and depends on the org.fedoraproject.Platform built from that same version of Fedora.

Aside: I should note that to do that you need OSTree to make the Platforms.

# Controlling updates 
Flatpak itself does not do any updates automatically. It relies on your software application to do it (Gnome Software).  It also has the ability for apps to check for their [own updates and ask to update](https://blogs.gnome.org/mclasen/2019/12/19/9100/) itself.

Snaps are more complicated, but why? Let's look at the [Ubuntu IoT and device services](https://ubuntu.com/internet-of-things/appstore) that Canonical sells:

**Dedicated app store**
...complete control of application versions, updates and controlled rollouts for $15,000 per year.

**Enterprise app store**
...control snap updates and upgrades. Ensure that all device traffic goes through an audited communications channel and determine the precise versions of snaps used inside the business.

Control of the update process is one of the ways Canonical is trying to make money. I don't believe anyone has ever told me explicitly that this is why Snaps update work this way. it just makes sense given the business considerations.

# So who is going to "win"?
One of them might go away, but neither is set to become *the* universal way to get apps on Linux at least not today.

It could change starting with something like:

 * Flatpak (or something like it) evolves to support command line or other apps.
 * A [snap based Ubuntu desktop](https://github.com/canonical/ubuntu-core-desktop) takes off and becomes the default Ubuntu.

Either isn't going to get it all the way there, but is needed to prove what the technology can do.
In both cases, the underlying confinement technology is being improved for all.

# Comments
Maybe I missed something? Feel free to make a [PR to add comments!](https://gitlab.com/BryanQuigley/bryanquigley.com/-/blob/master/posts/2021-flatpak-vs-snap.md)