<!--
.. title: PipeWire plays it
.. slug: pipewire-it-plays
.. date: 2021-02-01 19:14:12+00:00
.. tags: 
.. category: mindshare
.. link: 
.. description: 
.. type: text
-->

I'm running Debian 11 (testing) with XFCE and getting PipeWire up and running was relatively easy - although [explicitly unsupported](https://salsa.debian.org/utopia-team/pipewire/-/commit/a70b91c2a4af80c085c192a8642ecd5988d2b1a7) for Debian 11.

```shell
sudo apt install pipewire pipewire-audio-client-libraries
sudo apt remove pulseaudio pulseaudio-utils
sudo apt autoremove
```

At some future point there will be something like pipewire-pulse which will do the rest, but for now you must:
```shell
sudo touch /etc/pipewire/media-session.d/with-pulseaudio
sudo cp /usr/share/doc/pipewire/examples/systemd/user/pipewire-pulse.* /etc/systemd/user/
systemctl --user enable pipewire-pulse pipewire
```
I suggest a reboot after, but a logout may be enough.  Then try playing some music. If it worked, it should play just like it has before.

# More processes

```shell
1456 bryan     20   0 1023428 102436  50396 S   1.7   2.6   0:02.06 quodlibet                     
690 bryan      9 -11  898044  27364  20932 S   1.0   0.7   0:00.31 pulseaudio
```

PipeWire runs as 3 separate processes compared to PulseAudio above. Of note, apparently PipeWire does want to adjust it's nice level, but in it's current state it doesn't depend on it - and I haven't seen any need for it.

```shell
PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                       
936 bryan     20   0  826812 100484  50472 S   1.3   2.5   0:02.71 quodlibet                     
692 bryan     20   0   94656  12480   5928 S   0.7   0.3   0:00.38 pipewire-pulse                
693 bryan     20   0  107408  15228   7192 S   0.3   0.4   0:00.39 pipewire
701 bryan     20   0  225340  22756  17280 S   0.0   0.6   0:00.06 pipewire-media-
```
# What's works? Everything so far..

 * Playing music locally
 * Playing videos locally 
 * Playing music/videos on the web
 * Video calls via Jitsi
 * Changing volume using xfce's pulseaudio applet
 
**Except** I can't change individual application volumes because pavucontrol was removed. I belive pavucontrol could actually control it, but I haven't tried it.

# So worth switching?
If you want to be an early adopter, jump on in. If not [Fedora](https://fedoraproject.org/wiki/Changes/DefaultPipeWire) and Ubuntu will both be including it this year (although I'm not sure if Ubuntu will replace PulseAudio with it).

This is my favorite line from the Fedora proposal:
"...with both the PulseAudio and JACK maintainers and community. PipeWire is considered to be the successor of both projects."

It's generally a lot of work to get three projects to agree on standards between them, much less to have general agreement on a future path. I'm very impressed with all three groups to figure out a path to improve Linux audio together.