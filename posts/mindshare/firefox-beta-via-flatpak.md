<!--
.. title: Firefox Beta via Flatpak
.. slug: firefox-beta-via-flatpak
.. date: 2020-07-24 21:20:49 UTC
.. tags: firefox, flatpak
.. category: mindshare
.. link: 
.. description: 
.. type: text
-->

### What I've tried.
 1. Firefox beta as a snap.  (Definitely easy to install. But not as quick and harder to use for managing files - makes it's own Downloads directory, etc)
 2. Firefox (stock) with custom AppArmor confinement. (Fun to do once, but the future is clearly using [portals](https://github.com/flatpak/xdg-desktop-portal) for file access, etc)
 3. Firefox beta as a Flatpak. 

I've now been running Firefox as a Flatpak for over 4 months and have not had any blocking issues.

### Getting it installed

Flatpak - already installed on Fedora SilverBlue (comes with Firefox with some Fedora specific optimizations) and EndlessOS at least

Follow [Quick Setup](https://flatpak.org/setup/).  This walks you through installing the Flatpak package as well as the Flathub repo. Now you could easily install Firefox with just 'flatpak install firefox' if you want the Stable Firefox.

To get the beta you need to add the [Flathub Beta repo](https://blogs.gnome.org/alexl/2019/02/19/changes-in-flathub-land/). You can just run:
```shell
sudo flatpak remote-add flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
```

Then to install Firefox from it do (You can also choose to install as a user and not using sudo with the --user flag):
```text
sudo flatpak install flathub-beta firefox
```

Once you run the above commend it will ask you which Firefox to install, install any dependencies, tell you the permissions it will use, and finally install.
```text
Looking for matches…
Similar refs found for ‘firefox’ in remote ‘flathub-beta’ (system):

   1) app/org.mozilla.firefox/x86_64/stable
   2) app/org.mozilla.firefox/x86_64/beta

Which do you want to use (0 to abort)? [0-2]: 2
Required runtime for org.mozilla.firefox/x86_64/beta (runtime/org.freedesktop.Platform/x86_64/19.08) found in remote flathub
Do you want to install it? [Y/n]: y

org.mozilla.firefox permissions:
    ipc                          network       pcsc       pulseaudio       x11       devices       file access [1]       dbus access [2]
    system dbus access [3]

    [1] xdg-download
    [2] org.a11y.Bus, org.freedesktop.FileManager1, org.freedesktop.Notifications, org.freedesktop.ScreenSaver, org.gnome.SessionManager, org.gtk.vfs.*, org.mpris.MediaPlayer2.org.mozilla.firefox
    [3] org.freedesktop.NetworkManager


        ID                                             Branch            Op            Remote                  Download
 1. [—] org.freedesktop.Platform.GL.default            19.08             i             flathub                    56.1 MB / 89.1 MB
 2. [ ] org.freedesktop.Platform.Locale                19.08             i             flathub                 < 318.3 MB (partial)
 3. [ ] org.freedesktop.Platform.openh264              2.0               i             flathub                   < 1.5 MB
 4. [ ] org.gtk.Gtk3theme.Arc-Darker                   3.22              i             flathub                 < 145.9 kB
 5. [ ] org.freedesktop.Platform                       19.08             i             flathub                 < 238.5 MB
 6. [ ] org.mozilla.firefox.Locale                     beta              i             flathub-beta             < 48.3 MB (partial)
 7. [ ] org.mozilla.firefox                            beta              i             flathub-beta             < 79.1 MB
```

The first 5 dependencies downloaded are required by most applications and are shared, so the actual size of Firefox is more like 130MB.

### Confinement
 * You can't browsing for local files via browser file:/// (except for ~/Downloads).  All local files need to be opened by Open File Dialogue which automatically adds the needed permissions.
 ![Unboxing](/images/2020-07-22-Firefox-flatpak/confined-home.png)
 * You can enable Wayland as well with 'sudo flatpak override --env=GDK_BACKEND=wayland org.mozilla.firefox  (Wayland doesn't work with the NVidia driver and Gnome Shell in my setup though)
 
### What Works?
Everything I want which includes in no particular order:

 * Netflix (some older versions had issues with DRM IMU)
 * WebGL (with my Nvidia card and proprietary driver. Flatpak installs the necessary bits to get it working based on your video card)
 * It's speedy, it starts quick as I would normally expect
 * Using the file browser for ANY file on my system.  You can upload your private SSH keys if you really need to, but you need to explicitly select the file (and I'm not sure how you unshare it).
 * Opening apps directly via Firefox (aka I download a PDF and I want it to open in Evince - this does use portals for confinement).
 * Offline mode

### What could use work?
 * Some flatpak commands can figure out what just "Firefox" means, while others want the full org.mozilla.firefox
 * If you want to run Firefox from the command line, you need to run it as org.mozilla.firefox.  This is the same for all Flatpaks, although you can make an alias.
 * It would be more convenient if Beta releases were part of the main Flathub (or advertised more)
 * If you change your Downloads directory in Firefox, you have to update the permissions in Flatpak or else it won't allow it to work.  If you do Save As.. it will work fine though.
 * The flatpak permission-* commands lets you see what permissions are defined, but [resetting or removing](https://github.com/flatpak/flatpak/issues/3759) doesn't seem to actually work.
 
If you think you found a Flatpak specific Mozilla bug, the first place to look is [Mozilla Bug #1278719](https://bugzilla.mozilla.org/show_bug.cgi?id=1278719) as many bugs are reported against this one bug for tracking purposes.

# Comments
Add a comment by making a [Pull Request](https://gitlab.com/BryanQuigley/bryanquigley.com/-/blob/master/posts/mindshare/firefox-beta-via-flatpak.md) to this post.
