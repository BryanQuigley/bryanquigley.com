<html><body><p>A follow up from my <a href="http://bryanquigley.com/mindshare/browsers-and-how-they-show-trust-and-encyption">previous post</a>;
</p><h3 style="margin-bottom: 0in;">Firefox 12 Currently</h3>
<p style="margin-bottom: 0in;">...has something like this to show trust and encryption (the colors are off but hopefully you get the idea, the actual blue and green are much nicer on the eyes):
<span style="background: #579d1c;">[V] The Vanguard Group Inc, (US)  </span><span style="color: #999999;"> https://personal.</span><span style="color: #000000;">vanguard.com</span><span style="color: #999999;">/us/CorporatePortal</span></p>
<p style="margin-bottom: 0in;"><span style="background: #0099ff;">[d]duckduckgo.com  </span><span style="color: #999999;"> https://</span>duckduckgo.com<span style="color: #999999;">/?q=cheese</span></p>
<p style="margin-bottom: 0in;"><span style="background: #0099ff;">[g]google.com  </span><a href="https://encrypted.google.com/"><span style="color: #999999;"><span style="background: transparent;"> https://encrypted.</span></span><span style="color: #000000;"><span style="background: transparent;">google.com</span></span></a></p>
<p style="margin-bottom: 0in;"><span style="color: #000000;"><span style="background: transparent;">[/.] slashdot.org</span></span></p>

<h3><span style="color: #000000;"><span style="background: none repeat scroll 0% 0% transparent;">My proposal:</span></span></h3>
<p style="margin-bottom: 0in;"><span style="background: #579d1c;">[V] The Vanguard Group Inc. (US)  </span><span style="color: #000000;"><span style="background: #0099ff;">  personal.<strong>vanguard.com </strong></span></span><span style="color: #000000;">  /us/CorporatePortal</span></p>
<p style="margin-bottom: 0in;"><span style="background: transparent;">[d] </span><strong><span style="background: none repeat scroll 0% 0% #0099ff;">duckduckgo.com </span></strong>  /?q=cheese<span style="background: #0099ff;">
</span></p>
<p style="margin-bottom: 0in; background: transparent;"><span style="background: transparent;">[g] </span><span style="background: #0099ff;">encrypted.<strong>google.com</strong></span><strong><span style="background: none repeat scroll 0% 0% transparent;">
</span></strong></p>
<p style="margin-bottom: 0in;"><span style="color: #000000;"><span style="background: transparent;">[/.] <strong>slashdot.org</strong></span></span></p>
<p style="margin-bottom: 0in;">I'm curious if you can figure out what everything means in my proposal without explanation.</p>

<h3 style="margin-bottom: 0in;">Explanation</h3>
<ul>
 	<li>Green is for trust and only for trust.  Notice how the favicon is only colored at all when using Extended Validation.  AFAIK it should never be a domain name.</li>
 	<li>Blue is for encrypted and only for encrypted, and only used for the sub+domain name.  I'm hoping this will provide a non-color cue for those who are colorblind, to differentiate between the two.</li>
 	<li>I got rid of the greying out of text and moved to a bolding of the domain name, this helped due to my bad green/blue colors but might not be necessary in the real version</li>
 	<li>Spacing between the domain name and the rest of the url to help keep them even more separate in a quick glance</li>
 	<li>Oh, and the complete lack of https/http, I would want to see Opera's awesome feature implemented where they hide them unless you click on the URL bar.</li>
</ul>
My overall goal was to try to communicate both a level of trust and a level of encryption, while making it easy at a glance.  In addition, giving us the option in the future to really separate these two concepts.

Looking for suggestions, comments, and feedback before I try to propose it to Mozilla.  Check out my <a title="Browsers and how they show Trust and Encyption" href="http://bryanquigley.com/mindshare/browsers-and-how-they-show-trust-and-encyption">previous blog post</a> for what they are actually planning to do for Firefox 14.

<strong>Comments</strong>

 
<ul>
 	<li id="comment-1424" class="comment even thread-even depth-1 parent"><article id="div-comment-1424" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn"><a class="url" href="http://www.florian-diesch.de" rel="external nofollow">Florian Diesch</a></b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-01T04:49:02+00:00"> May 1, 2012 at 4:49 am </time></div>
</footer>
<div class="comment-content">

The focusing on the certificate name is nice for the certification authorties but doesn’t increase security that much as long as it’s not clear what proof of identity the certification authority requires.

I guess most users will not spot the difference between “The Vanguand Group Inc. (US)” and “The Vanguard Group Inc. (US)”. Marking a connection to “personal.vanguand.com” as “trusted” helps someone who got this domain and certificate to pretent to be Vanguard.

In my opinion for most use cases the important information is “that’s the same site I used before”. Therefor it would be nice if the user could assign some visual clue (e.g. an icon) to a certificaten and have the browser to display that clue everytime this certificate is used.

So if the user assigns some cute kitten icon with his bank’s web site he’ll easily know “No cute kitten –&gt; NOT MY BANK!”.

This would even work for web sites with a certificate that’s not from a certification authority known by the browser.

</div>
</article>
<ol class="children">
 	<li id="comment-1425" class="comment byuser comment-author-bryan bypostauthor odd alt depth-2"><article id="div-comment-1425" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-01T04:53:42+00:00"> May 1, 2012 at 4:53 am </time></div>
</footer>
<div class="comment-content">

For them to get Vanguand Group they would actually need to be incorporated as the Vanguand group in the US. I’m pretty sure, Vanguard would sue them before they can get that. – That’s the big benefit of EV.

I totally agree that we should incorporate the user’s sense of trust into it somehow. User selected site icons is an interesting idea.. I was also thinking of some sort of First Visit notice…

</div>
</article></li>
</ol>
</li>
 	<li id="comment-1427" class="comment even thread-odd thread-alt depth-1"><article id="div-comment-1427" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">foo</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-01T07:09:41+00:00"> May 1, 2012 at 7:09 am </time></div>
</footer>
<div class="comment-content">

The meaning of colours is *highly* culturally determined, red doesn’t always mean bad, green doesn’t always mean good. I strongly suggest not doing that.

</div>
</article></li>
 	<li id="comment-1430" class="comment odd alt thread-even depth-1 parent"><article id="div-comment-1430" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Oxwivi</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-01T10:42:16+00:00"> May 1, 2012 at 10:42 am </time></div>
</footer>
<div class="comment-content">

I second! Furthermore, with the favicons being featured on tabs as well, it’d be more useful to boot the favicon form the address bar and replace it with an icon with the sole purpose of indicating if the connection is encrypted or not (would complement your idea as it does not show <a href="https://" rel="nofollow">https://</a>), a la Opera.

By the way, I’m curious as to how we can propose features, changes and such to Mozilla. Is it the same as other projects (mailing list, IRC, etc)?

</div>
</article>
<ol class="children">
 	<li id="comment-1431" class="comment even depth-2"><article id="div-comment-1431" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Oxwivi</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-01T10:49:52+00:00"> May 1, 2012 at 10:49 am </time></div>
</footer>
<div class="comment-content">

Oh, and the icon should have three states: one of them being mixed content (obtained through both secure and unsecure connections); the last two are the trust and encryption as you describe.

</div>
</article></li>
</ol>
</li>
 	<li id="comment-1450" class="comment odd alt thread-odd thread-alt depth-1 parent"><article id="div-comment-1450" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn"><a class="url" href="https://msujaws.wordpress.com" rel="external nofollow">Jared Wein</a></b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-03T03:57:39+00:00"> May 3, 2012 at 3:57 am </time></div>
</footer>
<div class="comment-content">

It’s interesting that you decided to keep the favicon in the address bar. In your previous post you mentioned that there isn’t a great deal of difference between having favicons in the address bar or on the tab, and that users can and will look in the wrong place (which I agree with).

Have you seen the mockups that Alex Faaborg did a while ago for reducing redundancy in the address bar? It is very similar to your design here. Basically, the goal was to move to the site-identity block to be the effective hostname. Here is a link to the mockup in case you are interested: <a href="https://bug588270.bugzilla.mozilla.org/attachment.cgi?id=466899" rel="nofollow">https://bug588270.bugzilla.mozilla.org/attachment.cgi?id=466899</a>

Implementing the design that you have proposed brings with it some technical challenges. Bolded fonts are rendered differently on all the major OSes, and we would need to figure out how to transition from the colored and spaced out URL to a plaintext URL when the user gives focus to the address bar. None of these issues make this a bad idea, but they would need to be figured out before something like this could be shipped.

Thanks for writing up the proposal, I think it is very well thought out 🙂

</div>
</article>
<ol class="children">
 	<li id="comment-1456" class="comment byuser comment-author-bryan bypostauthor even depth-2"><article id="div-comment-1456" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bryan</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-04T01:59:05+00:00"> May 4, 2012 at 1:59 am </time></div>
</footer>
<div class="comment-content">

On the favicon vs lock icon, I went with favicon, because I don’t want to reinforce that a lock means they are safe.

I really like those mockups, and I think from seeing them I understand more of why switch to a lock/web at all. If it’s part of a transition to using that space for the different activation that makes some sense.

I didn’t realize bold could have so many problems…

</div>
</article></li>
</ol>
</li>
 	<li id="comment-1551" class="comment odd alt thread-even depth-1"><article id="div-comment-1551" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">JanC</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-05-19T18:40:18+00:00"> May 19, 2012 at 6:</time><time datetime="2012-05-19T18:40:18+00:00">40</time><time datetime="2012-05-19T18:40:18+00:00"> pm </time></div>
</footer>
<div class="comment-content">

The problem is that if you want to show trust, you need a way to audit what CA to trust. The CA I have seen to be trustworthy only cover a very small share of the market (try removing Symantec/Verisign from you browser as “trusted” CA–because they are not trustworthy–if you want to see the effect of making SSL/TLS really secure to common users).

</div>
</article></li>
</ul>
 </body></html>