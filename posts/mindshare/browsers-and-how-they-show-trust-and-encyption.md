<html><body><p>Firefox is moving ahead with a new way to convey the security level of websites...See the differences for yourself.  Also including Chrome and Opera for comparison.

Key: <strong>HTTP</strong> is the plain old web, no guarantee of well anything really security wise
<strong>HTTPS</strong> your communication is encrypted to a specific website
<strong>HTTPS with EV</strong> or Extended Validation ensures your communication is encrypted to a specific website AND that company X (at so and so address, and incorporated in Y) owns said website.
</p><table border="”1″" width="”390″" cellspacing="”0″" cellpadding="”0″">
<tbody>
<tr>
<td>Firefox 12 (current)</td>
<td> HTTP</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Firefox12_Nothing.png"><img title="Firefox12_Nothing" src="http://bryanquigley.com/wp-content/uploads/2012/04/Firefox12_Nothing.png" alt="" width="576" height="54"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Firefox12_BasicSSL.png"><img class=" wp-image-646 alignnone" title="Firefox12_BasicSSL" src="http://bryanquigley.com/wp-content/uploads/2012/04/Firefox12_BasicSSL.png" alt="" width="670" height="55"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS with EV</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Firefox12_EV.png"><img class="alignnone wp-image-647" title="Firefox12_EV" src="http://bryanquigley.com/wp-content/uploads/2012/04/Firefox12_EV.png" alt="" width="765" height="53"></a></td>
</tr>
<tr>
<td>Firefox 14  (new)</td>
<td>HTTP</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/FirefoxAurora_Nothing.png"><img title="FirefoxAurora_Nothing" src="http://bryanquigley.com/wp-content/uploads/2012/04/FirefoxAurora_Nothing.png" alt="" width="625" height="54"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/FirefoxAurora_BasicSSL.png"><img class="wp-image-655 alignleft" title="FirefoxAurora_BasicSSL" src="http://bryanquigley.com/wp-content/uploads/2012/04/FirefoxAurora_BasicSSL.png" alt="" width="768" height="57"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS with EV</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/FirefoxAurora_EV.png"><img class=" wp-image-656 alignnone" title="FirefoxAurora_EV" src="http://bryanquigley.com/wp-content/uploads/2012/04/FirefoxAurora_EV.png" alt="" width="780" height="57"></a></td>
</tr>
<tr>
<td>Opera</td>
<td>HTTP</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Opera_Nothing.png"><img class="alignnone size-full wp-image-678" title="Opera_Nothing" src="http://bryanquigley.com/wp-content/uploads/2012/04/Opera_Nothing.png" alt="" width="390" height="59"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Opera_SSL.png"><img class="alignnone size-full wp-image-679" title="Opera_SSL" src="http://bryanquigley.com/wp-content/uploads/2012/04/Opera_SSL.png" alt="" width="418" height="61"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS with EV</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Opera_EV.png"><img class="alignnone size-full wp-image-677" title="Opera_EV" src="http://bryanquigley.com/wp-content/uploads/2012/04/Opera_EV.png" alt="" width="621" height="58"></a></td>
</tr>
<tr>
<td>Chrome</td>
<td>HTTP</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Chrome_Nothing.png"><img class="alignnone size-full wp-image-687" title="Chrome_Nothing" src="http://bryanquigley.com/wp-content/uploads/2012/04/Chrome_Nothing.png" alt="" width="418" height="61"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Chrome_HTTPS.png"><img class="alignnone size-full wp-image-686" title="Chrome_HTTPS" src="http://bryanquigley.com/wp-content/uploads/2012/04/Chrome_HTTPS.png" alt="" width="502" height="61"></a></td>
</tr>
<tr>
<td></td>
<td>HTTPS with EV</td>
<td><a href="http://bryanquigley.com/wp-content/uploads/2012/04/Chrome_EV.png"><img class="alignnone size-full wp-image-685" title="Chrome_EV" src="http://bryanquigley.com/wp-content/uploads/2012/04/Chrome_EV.png" alt="" width="676" height="63"></a></td>
</tr>
</tbody>
</table>
Additional Notes:
Opera doesn't warn on <a href="https://www.eff.org/https-everywhere/deploying-https">Mixed HTTP/HTTPS Content</a>, instead it just displays it as "Web" (no security markers) which certainly puts security first.
Opera - when you click the url bar, the full URL get's displayed including http:// or https://, otherwise they are usually hidden except for trusted sites for some reason.   That actually makes me like hiding http/https by default.

Which do you think of the above is the worst?   The best?  Why?

For me, Opera would win if they didn't have the lock symbol.  Saying outright "Secure" or "Trusted" I think works quite well. Otherwise I still really like Firefox 12.  It is quite easy to teach to people (I teach a Firefox course every other month or so), and works quite well at a glance.

The other goal of the Firefox 14 change was to "reduce some visual weight".  Which I read as make what kind of page (secure/etc) stand out less.  In fact, out of the above Firefox 14 is my last choice.
<h3>The Lock Symbol</h3>
It provides a false sense of security. If you tell people that a lock symbol means they are secure they are more likely to trust locks that are on the page or part of the favicon. Regardless if it's not displayed in the URL bar, it would still be on the tab.  It's not a big leap for a user to mistake one for the other.

The lock symbol does exist in Firefox 12, it will show up if you click on the Green or Blue bar. This keeps it from being something the user expects to see on the page though.
<h3>Links</h3>
You can read more about the reasoning behind the Firefox 14 change here: <a href="https://msujaws.wordpress.com/2012/04/23/an-update-to-site-identity-in-desktop-firefox/">https://msujaws.wordpress.com/2012/04/23/an-update-to-site-identity-in-desktop-firefox/</a>

Discussion about the return of the lock:<a href="https://groups.google.com/forum/#!topic/mozilla.dev.apps.firefox/ODX1PJutsLM/discussion">https://groups.google.com/forum/#!topic/mozilla.dev.apps.firefox/ODX1PJutsLM/discussion</a>

 

<strong>Comments</strong>

<strong>MarkC</strong>

I have no problem with Mozilla’s decision to remove the favicon from the address bar in Firefox 14, but I think losing the coloured backgrounds is a mistake.

It’s a lot easier to teach users to differentiate between blue, green and no colour than to explain the more subtle text/icon differences in the Firefox 14 screenshots.</body></html>