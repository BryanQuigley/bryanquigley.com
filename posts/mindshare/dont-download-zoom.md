<!--
.. title: Don't Download Zoom!
.. slug: dont-download-zoom
.. date: 2020-06-23 21:26:12+00:00
.. tags: 
.. category: mindshare
.. link: 
.. description: 
.. type: text
-->

**First**, I strongly recommend switching to [Jitsi Meet](https://meet.jit.si/):

 * It's free
 * It doesn't require you to sign up at all
 * It's open source
 * It's on the cutting edge of privacy and security features
 
**Second**,  Anything else that runs in a browser instead of trying to get you to download an specific desktop application.  Your browser protects you from many stupid things a company may try to do. Installing their app  means you are at more risk. (Apps for phones is a different story.).

A small sampling of other web based options:

 * [Talky.io](https://talky.io)  (also open source, no account required)
 * [8x8.vc](https://8x8.vc/) which is the company that sponsors Jitsi Meet.  Their offering has more [business options](https://www.8x8.com/products/video-conferencing)
 * Whatever Google calls their video chat product this week (Duo, Hangouts, Meet).
 * join.me
 * Microsoft Skype (no signups or account required for a basic meeting!)
 * whereby

There are many reasons not to choose [Zoom](https://en.wikipedia.org/wiki/Zoom_(software)#Criticism).

😞😞😞

**Finally**, So you have to use Zoom?

Zoom actually supports joining a call with a web browser.  They just don't promote it.  Some things may not work as well but you get to keep more of your privacy and security.

0. On joining the meeting close the request to run a local app.
0. Click Launch Meeting in middle of screen.
![Zoom join meeting page](/images/2020-06-23-zoom/ZoomIntro.png)
0. Again close out of the request to open a local app
0. Ideally, you now get a join from browser, click that!
![Click join from browser](/images/2020-06-23-zoom/ClickJoinFromBrowser.png)

If it doesn't work try loading the site in another browser. First try Chrome (or those based on it - Brave/Opera) and then Firefox.  It's possible that your organization may have disabled the join from web feature.

If you are a Zoom host or admin (why?) you can also ensure that the web feature is not [disabled](https://support.zoom.us/hc/en-us/articles/115005666383-Show-a-Join-from-your-browser-Link#h_be07f8e5-a650-42fe-bd78-ba6e2eec53c1).
