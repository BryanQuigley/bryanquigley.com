<!--
.. title: Lubuntu Memory Usage and Rsyslog
.. slug: 2023-lubuntu-focus
.. date: 2023-11-25 02:42:05+00:00
.. tags: Memory Usage
.. category:    
.. link: 
.. description: 
.. type: text
-->

In 2020 I reviewed [LiveCD memory usage](/posts/memory-usage/2020-livecd-memory-usage-compare.html).

I was hoping to review either Wayland only or immutable only (think ostree/flatpak/snaps etc) but for various reasons on my setup it would just be a Gnome compare and that's just not as interesting. There are just to many distros/variants for me to do a full followup.

Lubuntu has previously always been the winner, so let's just see how Lubuntu 23.10 is doing today.

Previously in 2020 Lubuntu needed to get to 585 MB to be able to run something with a livecd. With a fresh install today Lubuntu can still launch Qterminal with just 540 MB of RAM (not apples to apples, but still)!  And that's without Zram that it had last time.

I decided to try removing some parts of the base system to see the cost of each component (with 10MB accuracy).  I disabled networking to try and make it a fairer compare.
 
 * Snapd -  30 MiB
 * Printing - cups* foomatic*  - 10 MiB
 * rsyslog/crons - 10 MiB

# Rsyslog impact
Out of the 3 above it's felt more like with rsyslog (and cron) are redundant in modern Linux with systemd. So I tried hitting the log system to see if we could get a slowdown, by every .1 seconds having a service echo lots of gibberish.

After an hour of uptime, this is how much space was used:

 * syslog 575M
 * journal at 1008M

CPU Usage on fresh boot after:

With Rsyslog

 * gibberish service was at 1% CPU usage
 * rsyslog was at 2-3%
 * journal was at ~4%

Without Rsyslog

 * gibberish service was at 1% CPU usage
 * journal was at 1-3%

 That's a pretty extreme case, but does show some impact of rsyslog, which in most desktop settings is redundant anyway.

Testing notes:

 * 2 CPUs (Copy host config)
 * Lubuntu 23.10 install
 * no swap file
 * ext4, no encryption
 * login automatically
 * Used Virt-manager and only default change was enabling EUFI

