<!--
.. title: 2024 LiveCD Memory Usage Compare
.. slug: 2024-livecd-memory-usage-compare
.. date: 2024-10-20 00:54:00+00:00
.. tags: firefox, Memory Usage
.. category: 
.. link: 
.. description: 
.. type: text
-->

I am using pretty much the exact same setup I did in [2020](2020-livecd-memory-usage-compare.html). Let's see who is more efficient in a live session!

But first let's take a look at the image sizes:

{{% chart Bar title='Image size (in G)'
x_label_rotation=20
x_labels='[ "Ubuntu","Xubuntu","Xubuntu-minimal", "Kubuntu","Lubuntu","Ubuntu Mate","Manjaro 24.1 (KDE)", "Linux Mint 22 (Cinnamon)","Fedora 40 (Gnome)","Endless OS 6" ]' %}}
        '', [5.8  ,    3.9        , 2.5          , 4.1     , 3.1     , 4.0         , 3.9            , 2.8                  , 2.2               , 3.9     ]
{{% /chart %}}

[Charge Open Movie](https://www.youtube.com/watch?v=UXqq0ZvbOnk) is what I viewed if I can make it to YouTube.

I decided to be more selective and remove those that did very porly at 1.5G, which was most.

 * Ubuntu - booted but desktop not stable, took 1.5 minutes to load Firefox
 * Xubuntu-minimal - does not include a web browser so can't further test. Snap is preinstaled even though no apps are - but trying to install a web browser worked but couldn't start.
 * Manjaro KDE - Desktop loads, but browser doesn't
 * Xubuntu - laggy when Firefox is opened, can't load sites
 * Ubuntu Mate -laggy when Firefox is opened, can't load sites
 * Kubuntu - laggy when Firefox is opened, can't load sites
 * Linux Mint 22 - desktop loads, browsers isn't responsive

{{% chart Bar title='Memory usage compared (in G)'
x_label_rotation=20
legend_at_bottom=True
x_labels='                             [ "Lubuntu","Endless OS 6.0","Fedora 40", ]' %}}
'Desktop responsive',                  [ .45      ,    1           , 0.7        ],
'Web browser loads simple site',       [ 0.9      ,    1           , 1.1        ],
'YouTube worked fullscreen',           [ 1.1      ,    1.3         , 1.4        ],
{{% /chart %}}

Fedora video is a bit laggy, but watchable.. EndlessOS with Chromium is the most smooth and resonsive watching YouTube.

For fun let's look at startup time with 2GB (with me hitting buttons as needed to open a folder)

{{% chart Bar title='Startup time (Seconds)'
x_label_rotation=20
x_labels='                 [ "Lubuntu","Endless OS 6.0","Fedora 40", ]' %}}
'Seconds',                 [ 33      ,    93           , 45        ],
{{% /chart %}}

# Conclusion

* Lubuntu lowered it's memory usage from 2020 for loading a desktop 585M to 450M! Kudos to Lubuntu team! 
* Both Fedora and Endless desktops worked in lower memory then 2020 too!
* Lubuntu, Fedora and Endless all used Zram.
* Chromium has definitely improved it's memory usage as last time Endless got dinged for using it. Now it appears to work better then Firefox.

Notes:

 * qemu-system-x86_64 -enable-kvm -cdrom lubuntu-24.04.1-desktop-amd64.iso  -m 1.5G -smp 4 -cpu host -vga virtio  --full-screen
 * Screen size was set to 1080p/60Hz.
 * I tried to reproduce 585M on Lubuntu 20.04 build, but it failed on anything below 1G.
 * Getting out of full screen on YouTube apparently is an intensive task.  Dropped testing that.
 * All Ubuntu was 24.04.1 LTS.
