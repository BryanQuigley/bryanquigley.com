<!--
.. title: 2020 LiveCD Memory Usage Compare
.. slug: 2020-livecd-memory-usage-compare
.. date: 2020-06-23 02:42:05+00:00
.. tags: firefox, Memory Usage
.. category: 
.. link: 
.. description: 
.. type: text
-->

Time for a 20.04 LTS LiveCD memory comparison with a bunch more distros.  I last did one in [2016](/posts/memory-usage/ubuntu-16-04-livecd-memory-usage-compared.html).  

Using Lubuntu as an example base memory usage approximately doubled from 2016 (251M) to 2020 (585M).   Those numbers aren't strictly comparable because I'm not using the exact same setup as in 16.04 and I enabled more modern features (virtio graphics, EUFI, 4 cores).

{{% chart Bar title='Memory usage compared (in G)'
x_label_rotation=20
legend_at_bottom=True
x_labels='[       "Clear 33300","Elementary 5.1","Endless 3.8","Fedora 32","Kubuntu","Lubuntu","Manjaro 20.0.3 XFCE","openSUSE Leap 15.1","Solus 4.1","Ubuntu","Ubuntu Budgie","Ubuntu Mate","Xubuntu"]' %}}
        'Boots to DE that can start something',   [0.8  , 0.8        , 1       , 1.25   , 0.8     , 0.585   , 0.9            , 1.25    , 1     , 1      , 1              , 0.9       , .6 ]
        'Browser load simple website', [1.5  , 1.25       , 1.5     , 1.5    , 1.25    , 0.7     , 1.5            , 1.75    , 1.5   , 1.5    , 1.5            , 1.25      , 1.25]
        'YouTube plays Big Buck Bunny maximized', [1.75 , 1.75       , 1.75    , 1.75   , 1.75    , 0.9     , 1.75           , 2       , 1.75  , 2.25   , 2              , 1.75      , 1.5]
        {{% /chart %}}

Lubuntu is able to work with less at least partially because of Zram. The other distro that has Zram enabled is Endless, but they also use the Chromium browser which generally uses more memory than Firefox (also Elementary uses Ephipany).  My guess is if Xubuntu enabled zram it's profile would more closely match Lubuntu.


Notes:

 * Time limit for each applicaton launch is approximately 30 seconds.
 * Accuracy over 1G is by .25G increments.  Under 1G, I tried to narrow it down to at least .1G.
 * Getting out of full screen on YouTube apparently is an intensive task.  Dropped testing that.
 * Screen size was set to 1080p/60Hz.
 * Sample qemu line: qemu-system-x86_64 -enable-kvm -cdrom clear-33300-live-desktop.iso -smbios file=/usr/share/ovmf/OVMF.fd -m 1024M -smp 4 -cpu host -vga virtio --full-screen
 * All Ubuntu derivatives were from 20.04 LTS




