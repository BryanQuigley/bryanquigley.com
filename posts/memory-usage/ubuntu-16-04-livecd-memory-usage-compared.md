<html><body><p>The latest Ubuntu LTS is out, so it's time for an updated memory usage comparison.

<a href="/wp-content/uploads/2016/05/1604MemoryCompare.png"><img class="alignnone size-full wp-image-2236" src="/wp-content/uploads/2016/05/1604MemoryCompare.png" alt="1604MemoryCompare" width="680" height="383"></a>

Boots means it will boot to a desktop that you can move the mouse on and is fully loaded.  While Browser and Smooth means we can load my website in a reasonable amount of time.
</p><h2><strong>Takeaways</strong></h2>
<strong>Lubuntu is super efficient</strong>

Lubuntu is amazing in how much less memory it can boot in.  I believe it is still the only one with ZRam enabled by default, which certainly helps a bit.

I actually did the memory usage for ZRam to the nearest MB for fun.
The 32 bit version boots in 224 MB, and is smooth with Firefox at only 240MB!   The 64 bit boots at only 25 MB more (251), but is needs 384 MB to be smooth.

<strong>If you are memory limited, change flavors first, 32-bit won't help that much</strong>

Looking just at "Browser and Smooth" because that's a more clear use-case.  There is no significant memory  difference between the 32 and 64 bit varients of: Xubuntu,  Ubuntu Gnome, Ubuntu (Unity).

Lubuntu, Kubuntu, and Ubuntu Mate do have significant deltas, which let's explore:
Kubuntu - If you are worried about memory requirements do not use.
Ubuntu Mate - It's at most a 128MB loss, likely less.  (We did that to 128MB accuracy).
Lubuntu 64 bit is smooth at 384MB.  32 bit saves almost 144 MB!  If you are severally memory limited 32-bit Lubuntu becomes your only choice.

<strong>Hard Memory Limit
</strong>The 32-bit hard memory requirement is 224 MB. (Below that is panics)
The 64-bit hard memory requirement is 251 MB.  Both of these were tested with Lubuntu.

Check out the <a href="https://bryanquigley.com/uncategorized/ubuntu-14-04-livecd-memory-usage-compared">14.04 Post</a>.   I used Virt-Manager/KVM instead of Virtualbox for the 16.04 test.

Extras: <a href="/wp-content/uploads/2016/05/memoryusage_16.04.txt">Testing Notes</a>,  <a href="/wp-content/uploads/2016/05/memoryusage_1604.ods">Spreadsheet</a></body></html>