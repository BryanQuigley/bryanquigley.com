<!--
.. title: Small EInk Phone
.. slug: small-eink-phone-results
.. date: 2022-05-22 04:30:45+00:00
.. type: text
-->

Aside in 2022-05-22. it's not the same.. but there is a renewed push by Pebble creator Eric Migicovsky to show demand for a [SmallAndroidPhone](https://smallandroidphone.com/). It's currently at about 29,000.

Update 2022-02-26: Only got 12 responses which likely means there isn't that much demand for this product at this time (or it wasn't interesting enough to spread). Here are the results as promised:

What's the most you would be willing to spend on this?
7 - $200, 4 - $400. But that doesn't quite capture it. Some wanted even cheaper than $200 (which isn't doable) and others were will to spend a lot more.

Of the priority's that got at least 2 people agreeing (ignoring rating):
4 - Openness of components, Software Investments
3 - Better Modem, Headphone Jack, Cheaper Price
2 - Convergence Capable, Color eInk, Replaceable Battery

I'd guess about half of the respondents would likely be happy with a PinePhone (Pro) that got better battery life and "Just Works".

End Update.

Would you be interested in crowdfunding a small E Ink Open Phone? If yes, check out the specs and fill out the form below.

If I get 1000 interested people, I'll approach manufacturers. I plan to share the results publicly in either case. I will never share your information with manufacturers but contact you by email if this goes forward.

Basics:

- Small sized for 2021 (somewhere between 4.5 - 5.2 inches)
- E Ink screen (Maybe Color) - battery life over playing videos/games
- To be shipped with one of the main Linux phone OSes (Manjaro with KDE Plasma, etc).
- Low to moderate hardware specs
- Likely >6 months from purchase to getting device

Minimum goal specs (we might be able to do much better than these, but again might not):

- 4 Core
- 32 GB Storage
- USB Type-C (Not necessarily display out capable)
- ~8 MP Front camera
- GPS
- GSM Modem (US)

Software Goals:

- Only open source apps pre-installed
- MMS/SMS
- Phone calls
- View websites / webapps including at least 1 rideshare/taxi service working (may not be official)
- 2 day battery life (during "normal" usage)

Discussions:
[Phoronix](https://www.phoronix.com/forums/forum/software/mobile-linux/1300018-small-e-ink-open-phone)
