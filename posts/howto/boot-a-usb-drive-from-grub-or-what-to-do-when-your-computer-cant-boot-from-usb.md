<html><body><p>I've ran into this situation before.. Had Linux installed on a machine and needed to reinstall/or install another distro. Fine, should be easy right? Load said distro on a USB stick and away we go. Unfortunately some (mostly older) machines can't boot from a USB stick. This annoys me (especially cause I really don't use writable CDs/DVDs at all anymore). It turns out if you already have GRUB1 installed on the machine, you can use that to boot the USB stick and even overwrite what's on the hard drive. This of course doesn't help you if you have no-OS or another OS -&gt; maybe look at PXE booting.   I'm still haven't gotten this to work with GRUB2 unfortunately, they've complicated things a little.  Still it's useful for old machines.
</p><ol>
 	<li>Figure out the kernel command line that your distro of choice uses (that you want to install/boot). I was using <a href="http://www.linuxmint.com/download_lmde.php">Linux Mint Debian</a> created by <a href="http://unetbootin.sourceforge.net/">Unetbootin</a>.
They (like many distros) store this in syslinux.cfg, look for the default label, and then note the items in bold.
<em>label unetbootindefault</em>
<em> menu label Default</em>
<em> <strong>kernel /ubnkern</strong></em>
<em> append <strong>initrd=/ubninit boot=live config live-media-path=/casper quiet splash --</strong></em></li>
 	<li>Boot target machine with USB stick inserted.  When GRUB appears Press Escape then C to get to the GRUB command line.</li>
 	<li>If you only have two drives in the machine the internal one will be (hd0,0) and the external one should be (hd1,0), or similar.
I typed the following, change with your version of the bolded information above, pressing enter after every command.
<em>root (hd1,0)</em>
<em> kernel /ubnkern</em>
<em> initrd /ubninit boot=live config live-media-path=/casper quiet splash</em></li>
 	<li>Ready to go?  Type boot and hit enter.</li>
</ol>
Tab auto-complete works great with GRUB..  You can do <code>root (hd</code> to get possible target drives, <code>root (hd0,</code> to get possible target partitions and filesystem types, and finally list possible kernel or initrd targets.

<strong>Comments</strong>

 
<ul>
 	<li id="comment-1335" class="comment even thread-even depth-1"><article id="div-comment-1335" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn"><a class="url" href="http://superkong.cat" rel="external nofollow">muzzol</a></b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-04-18T08:40:56+00:00"> April 18, 2012 at 8:40 am </time></div>
</footer>
<div class="comment-content">

i recommend to install Smart Boot Manager on MBR (sbm) and GRUB on first sector of partition.

sbm can detect on the fly external drives even on very old machines.

</div>
</article></li>
 	<li id="comment-1361" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-1361" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">kb</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2012-04-19T21:18:20+00:00"> April 19, 2012 at 9:18 pm </time></div>
</footer>
<div class="comment-content">

Way too complicated 🙂
I prefer the following grub.cfg and create the ISO with grub-mkrescue on Debian Squeeze. By the way, leaving off the partition like I do here is not documented as far as I can tell.

menuentry ‘First USB device’ {
root (hd1)
chainloader +1
}

menuentry ‘Second USB device’ {
#This is the fallback entry, so force the menu open when we fail as well:
set timeout=-1
root (hd2)
chainloader +1
}

#menuentry ‘Rescue partition (TODO)’ {
# set root='(hd0,1)’
# chainloader +1
#}

</div>
</article></li>
 	<li id="comment-5212" class="comment even thread-even depth-1"><article id="div-comment-5212" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Bruneti</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2014-10-10T14:14:14+00:00"> October 10, 2014 at 2:14 pm </time></div>
</footer>
<div class="comment-content">

Can I boot a recovery mac os X partition from grub prompt? I’ve got hfs+ on my iMac and grub tolds me unkwon filesystems … I need help ASAP!!!! thanks

</div>
</article></li>
</ul>
 </body></html>