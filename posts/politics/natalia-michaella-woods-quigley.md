<html><body><p>On June 19<sup>th</sup>, my fiancée and I returned from India.  She was 26 weeks pregnant and we returned to the States to get better health care for the delivery of our baby.  Health care can be a daunting thing to coordinate.  We used the health care broker CBIZ to set us up with a Blue Cross EPO Plan in NJ, with the specific requirement that the pregnancy be fully covered.

The problems started with the plan as soon as we arrived.  Although we had specifically said we needed the plan to be effective the day we arrived, it took 20+ days for the plan to be activated and useful for us.  During her 27<sup>th</sup> week of the pregnancy, contractions started and we were hospitalized.  We still hadn't even received health insurance cards yet.

The care we received at the University of Pennsylvania Hospital was excellent. Our daughter, Natalia, pulled through and we were allowed to go home a day earlier than expected.

We finally received our insurance cards.  Then, a couple of weeks later, we got news from the doctor’s office that Blue Cross wouldn’t cover our doctors office visits because we had a pre-existing condition.  We contacted CBIZ immediately and they said they would take care of it.  Two weeks later, we got an email asking for an additional requirement, basically saying if we didn't meet that requirement they couldn't take off the pre-existing clause off.  Back in May, they required that my fiancée had proof of coverage from before we got pregnant.  In August, after they’d been paid and coverage began, they changed the requirement to 1 year from date of the beginning of the Blue Cross plan, which we didn't have.

The best part of this so far is that we tried to be responsible.  We paid Blue Cross 300 dollars a month for this "coverage" on good faith that our pregnancy would be covered because we were advised against getting the free Medicare program by the CBIZ representative.  Nor were we told that you CANNOT apply for Medicare if you have insurance.

About 2 months ago, I went to work like any normal day, unaware that I would be at the hospital with my fiancée for the rest of the week.  Our Natalia hadn't made it.  (We delivered at Virtua Hospital in Voorhees, which, again, provided great care and as much comfort as they could given the circumstances)

The recovery and grief have been hard enough.  What’s worse, CBIZ has stopped returning our emails and calls.  My fiancée called Blue Cross about 10 times and got different stories every time she called, until she finally got a call with the same story about needing the 1 year prior coverage (this after getting told that the pre-existing clause would be removed).

About 2 weeks after our last hospital visit "Obamacare" as it is referred by pundits took effect.  If only it had kicked in 2 months ago.  He made pre-existing conditions a thing of the past, and we are very grateful that no one will have to go through that much health insurance hell again.  We don't know why our daughter didn't make it, but we are sure that the stress of dealing with health "insurance" definitely didn't help our well-being.

When I was getting ready to be a father one of the biggest things that scared me was that I was bringing a child into this world, with all of its horrific problems.  She didn't make it, but that feeling has stayed with me.   I will be volunteering with a local Democrat this election season, because the parts of this story that <i>we can help</i> we can't let happen again.

Our daughter, Natalia Michaella Woods-Quigley, was conceived around January 1<sup>st</sup> 2010.  She loved to listen to music (WXPN in Philly being her favorite radio station.)  Natalie loved eating all kinds of food (except Indian).  Her favorites were potatoes, pasta, cantaloupe, and burgers.  She was very opinionated, kicking her opinions every moment.  It's amazing how much (and how little) you can learn about someone when they aren't even born yet.  It's amazing how deeply you can fall in love with someone you can't even see.

We love you Natalie.
Bryan &amp; Erica

<strong>Comments</strong>

 
</p><ul>
 	<li id="comment-2024" class="comment even thread-even depth-1"><article id="div-comment-2024" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">shermann</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-10-27T18:51:24+00:00"> October 27, 2010 at 6:51 pm </time></div>
</footer>
<div class="comment-content">

&gt;Hi Bryan,

I know how you feel about your loss.
I'm sorry with you, but I can tell you, don't give up.

Best Regards,

Stephan

</div>
</article></li>
 	<li id="comment-2025" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2025" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Nathaniel Homier</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-10-27T19:09:21+00:00"> October 27, 2010 at 7:09 pm </time></div>
</footer>
<div class="comment-content">

&gt;You and your wife have my empathy. I'm glad she got the chance to live life even if only for a short time.

</div>
</article></li>
 	<li id="comment-2026" class="comment even thread-even depth-1"><article id="div-comment-2026" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Jan de Visser</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-10-27T19:16:32+00:00"> October 27, 2010 at 7:16 pm </time></div>
</footer>
<div class="comment-content">

&gt;Bryan,

I'm awefully sorry about your loss. We lost our little girl 7 years ago in December, at 26 weeks. The pain eventually goes away and we now have two healthy boys, but you never forget.

I wish you the best.

</div>
</article></li>
 	<li id="comment-2027" class="comment odd alt thread-odd thread-alt depth-1"><article id="div-comment-2027" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">Kevix</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-10-28T18:49:31+00:00"> October 28, 2010 at 6:49 pm </time></div>
</footer>
<div class="comment-content">

&gt;reading that passage from joy to sorrow to pain to death was heart wrenching. But you and your partner had that experience together and even if the outcome was unexpected, I don't think you would have chosen not to take that journey. May you try again when you are willing and the outcome a happier one.

</div>
</article></li>
 	<li id="comment-2028" class="comment even thread-even depth-1"><article id="div-comment-2028" class="comment-body"><footer class="comment-meta">
<div class="comment-author vcard"><b class="fn">gQuigs</b> <span class="says">says:</span></div>
<div class="comment-metadata"><time datetime="2010-10-29T01:18:56+00:00"> October 29, 2010 at 1:18 am </time></div>
</footer>
<div class="comment-content">

&gt;Thank you all for your kind words…
We both really appreciate them.

</div>
</article></li>
</ul>
 </body></html>