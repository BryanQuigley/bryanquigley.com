<!--
.. title: MPEG-2
.. slug: mpeg2-patent-tracker
.. date: 2020-08-06 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->


**Note: I am far from a patent lawyer...**

As of their January 1, 2024  [MPEG-2 Patent List](https://www.via-la.com/licensing-2/mpeg-2/mpeg-2-patent-list/), these
are the current patents the Via Licensing patent pool believe's are valid.

> -   [MY 141626-A](https://patents.google.com/patent/MY141626A/en) (Lapsed but still listed by MPEG-LA)
> -   [MY 163465-A](https://patents.google.com/patent/MY163465A/en) (possible expiration of 15 Sep 2032)
> -   [MY 177139-A](https://patents.google.com/patent/MY177139A/en) (possible expiration of 8 Sep 2035)


I've tried sending complaints to the Malaysia Patent office, but don't
believe anything will happen unless someone sues in court.

Does this still impact anyone? Yes, for instance Rapsberry Pis before 4
are still [not](https://github.com/raspberrypi/firmware/issues/939)
going to try let MPEG-2 be free until all countries are there. (MPEG-2
hardware support has been removed from Pi 4s)


MY 141626-A
===========

Title

:   SYNCHRONIZATION ARRANGEMENT FOR A COMPRESSED VIDEO SIGNAL

Client Reference

:   RP-010050

Received Date

:   18 Oct 2006

Application Number

:   PI 20064345

Grant Date

:   31 May 2010

OPI Date

:   13 Nov 1994

Filing Date

:   27 Apr 1994

Expiration Date

:   31 May 2025

Renewal Due Date

:   Please be aware to renew your patent before this date is reached, or
    else your patent will become expired. 30 May 2020

Agent

:   PATRICK MIRANDAH

Applicant / Owner

:   GE TECHNOLOGY DEVELOPMENT, INC., ONE INDEPENDENCE WAY (US)

Inventors

:   JOEL WALTER ZDEPSKI

International Patent Classification

:   H04N 7/56 . . . . Synchronising systems therefor 2006.01 30 Apr 2010

Priority

:   UNITED STATES OF AMERICA (US) 13 May 1993 060,924

Abstract

:   APPARATUS FOR DEVELOPING SYNCHRONIZATION OF AN INTERMEDIATE LAYER OF
    SIGNAL SUCH AS THE TRANSPORT OR MULTIPLEX LAYER OF A MULTI-LAYERED
    COMPRESSED VIDEO SIGNAL, INCLUDES AT THE ENCODING END OF THE SYSTEM
    APPARATUS (13, 23, 25) FOR INCLUDING A TIME STAMP REFERENCE, SUCH AS
    A COUNT VALUE FROM A MODULO K COUNTER (23), AND PROVISION FOR A
    DIFFERENTIAL TIME STAMP RELATED TO TIME STAMPS OF A FURTHER
    COMPRESSED SIGNAL OR DIFFERENTIAL TRANSIT TIMES OF RESPECTIVE
    TRANSPORT PACKETS. FLAGS ARE INCLUDED IN TRANSPORT PACKETS OF SIGNAL
    TO INDICATE THE PRESENCE OR ABSENCE OF THE TIME STAMPS OR
    DIFFERENTIAL TIME STAMPS. AT THE RECEIVING END OF THE SYSTEM,
    CIRCUITRY EXAMINES RESPECTIVE PACKETS FOR THE CONDITION OF THE FLAGS
    TO LOCATE SPECIFIC INFORMATION SUCH AS THE TIME STAMPS AND
    DIFFERENTIAL TIME STAMPS. A COUNTER (27) IS RESPONSIVE TO A
    CONTROLLED RECEIVER CLOCK SIGNAL AND THE COUNT VALUE OF THIS COUNTER
    IS SAMPLED AT THE ARRIVAL OF TRANSPORT PACKETS INCLUDING A FLAG
    INDICATING THE PRESENCE OF A TIME STAMP. CIRCUITRY IS DESCRIBED FOR
    USING THE TIME STAMPS AND THE SAMPLED COUNT VALUES OF THE RECEIVER
    COUNTER (36) TO PROVIDE A SIGNAL FOR CONTROLLING THE FREQUENCY OF
    THE RECEIVER CLOCK SIGNAL.FIGURE 3

Number of Claims

:   4

Number of pages of claims

:   4

Number of pages of drawing

:   5

Number of pages of full specification

:   9

MY 163465-A
===========

**Title**  SYNCHRONIZATION ARRANGEMENT FOR A COMPRESSED VIDEO SIGNAL
**Client Reference**   13876MY4/PM/ANA/HJJ
**Received Date**   04 Nov 2015
**Application Number**  PI 2015002705
**Grant Date**   15 Sep 2017
**OPI Date**  13 Nov 1994
**Filing Date**  24 Apr 1994
**Expiration Date**   15 Sep 2032

Renewal Due Date

:   Please be aware to renew your patent before this date is reached, or
    else your patent will become expired. 15 Sep 2024

Agent

:   PATRICK MIRANDAH

Applicant / Owner

:   GE TECHNOLOGY DEVELOPMENT, INC., ONE INDEPENDENCE WAY (US)

Inventors

:   JOEL WALTER ZDEPSKI

International Patent Classification

:   C09C 1/00 Treatment of specific inorganic materials other than
    fibrous fillers Preparation of carbon black 2006.01 11 Dec 2015 H03M
    7/30 . Compression Expansion Suppression of unnecessary data, e.g.
    redundancy reduction 2006.01 11 Dec 2015

Priority

:   UNITED STATES OF AMERICA (US) 13 May 1993 060,924

Abstract

:   Apparatus for developing synchronization of an intermediate layer of
    signal such as the transport or multiplex layer of a multi-layered
    compressed video signal, includes at the encoding end of the system
    apparatus (13,23,25) for including a time stamp reference, such as a
    count value from a modulo K counter (23), and provision for a
    differential time stamp related to time stamps of a further
    compressed signal or differential transit times of respective
    transport packets. Flags are included in transport packets of signal
    to indicate the presence or absence of the time stamps or
    differential time stamps. At the receiving end of the system,
    circuitry examines respective packets for the condition of the flags
    to locate specific information such as the time stamps and
    differential time stamps. A counter (27) is responsive to a
    controlled receiver clock signal and the count value of this counter
    is sampled at the arrival of transport packets including a flag
    indicating the presence of a time stamp. Circuitry is described for
    using the time stamps and the sampled count values of the receiver
    counter (36) to provide a signal for controlling the frequency of
    the receiver clock signal.

Number of Claims

:   16

Number of pages of claims

:   16

Number of pages of drawing

:   8

Number of pages of full specification

:   24



MY 177139-A
===========

**Title** SYNCHRONIZATION ARRANGEMENT FOR A COMPRESSED VIDEO SIGNAL
**Client Reference**   25053MY00005
**Received Date**   14 Sep 2017
**Application Number**  PI 2015002705
**Grant Date**   08 Sep 2020
**OPI Date**  24 Oct 1995
**Filing Date**  24 Apr 1994
**Expiration Date**   08 Sep 2035

Renewal Due Date

:    Please be aware to renew your patent before this date is reached, or else your patent will become expired. 08 Sep 2024

Agent

:   PATRICK MIRANDAH

Applicant / Owner

:   GE TECHNOLOGY DEVELOPMENT, INC., ONE INDEPENDENCE WAY (US)

Inventors

:   JOEL WALTER ZDEPSKI



Priority

:   UNITED STATES OF AMERICA (US) 13 May 1993	060,924

Abstract

:   Apparatus for developing synchronization of an intermediate layer of
    signal such as the transport or multiplex layer of a multi-layered
    compressed video signal, includes at the encoding end of the system
    apparatus (13,23,25) for including a time stamp reference, such as a
    count value from a modulo K counter (23), and provision for a
    differential time stamp related to time stamps of a further
    compressed signal or differential transit times of respective
    transport packets. Flags are included in transport packets of signal
    to indicate the presence or absence of the time stamps or
    differential time stamps. At the receiving end of the system,
    circuitry examines respective packets for the condition of the flags
    to locate specific information such as the time stamps and
    differential time stamps. A counter (27) is responsive to a
    controlled receiver clock signal and the count value of this counter
    is sampled at the arrival of transport packets including a flag
    indicating the presence of a time stamp. Circuitry is described for
    using the time stamps and the sampled count values of the receiver
    counter (36) to provide a signal for controlling the frequency of
    the receiver clock signal.

Number of Claims

:   5

Number of pages of claims

:   5

Number of pages of drawing

:   8

Number of pages of full specification

:   13





News log
========

2018-02-14 - Last US patent expired (Slashdot
[post](https://yro.slashdot.org/story/18/02/14/1621259/mpeg-2-patents-have-expired))

2023-12-27 - Removed references of [MY 128994](https://patents.google.com/patent/MY128994A/en), expired on 2022-03-30. Added MY 177139-A, appeared in current Via Licensing Patent List.

2024-02-24 - Rearranged and updated with lapsed new details.