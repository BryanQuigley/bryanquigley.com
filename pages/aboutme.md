<!--
.. title: About Me
.. slug: aboutme
.. date: 2020-08-06 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I have over 15 years general system administration experience and 10 years focused on open source software.  I'm happy diving into code, maintaining systems, creating policies, product management and more.

Me elsewhere:

 * [Github](https://github.com/BryanQuigley)
 * [Gitlab](https://gitlab.com/BryanQuigley)
 * [Launchpad](https://launchpad.net/~bryanquigley) - Site for Ubuntu/OpenStack bugs and package fixes

