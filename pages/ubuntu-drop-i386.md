<!--
.. title: Ubuntu i386 tracker 
.. slug: ubuntu-drop-i386
.. date: 2018-10-21
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

I've been maintaining this tracker of what flavors have dropped i386 from their images and figured sharing it might be helpful for someone.

# Log (Incomplete)
| Date     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;|  What happened |
| ---      |  ---           |
| 2013-06-13 | [Running Ubuntu/Unity on a machine that is not 64 bit capable?](/posts/crazy-ideas/running-ubuntuunity-on-a-machine-that-is-not-64-bit-capable.html) |
| 2013-07-21 | [Survey Results](/posts/crazy-ideas/survey-results.html) |
| 2014-10-21 | [Still running 32 bit Ubuntu?](/posts/crazy-ideas/still-running-32-bit-ubuntu.html) Survey. [Phoronix - Ubuntu 16.04 Might Be The Distribution's Last 32-Bit Release](https://www.phoronix.com/scan.php? page=news_item&px=MTgxOTQ) |
| 2014-11-07 | [32 bit usage - survey results](https://bryanquigley.com/posts/crazy-ideas/32-bit-usage-survey-results.html) |
| 2014-11-13 | [Ubuntu online summit - When should we stop making 32 bit images?](https://web.archive.org/web/20170331172527/https://summit.ubuntu.com/uos-1411/meeting/22353/when-should-we-stop-making-32-bit-images/) |
| 2016-02-02 | [Ubuntu Desktop on i386](https://lists.ubuntu.com/archives/ubuntu-devel-discuss/2016-February/016173.html) |
| 2016-06-28 | [xnox](http://blog.surgut.co.uk/) posted [Installation Media and supportability of i386 in 18.04 LTS Re: Ubuntu Desktop on i386](https://lists.ubuntu.com/archives/ubuntu-devel/2016-June/039420.html) |
| 2016-11-14 | [Scheduled discussion about ending i386 to happen soon](https://lists.ubuntu.com/archives/ubuntu-desktop/2016-November/004840.html) |
| 2017-09-27 | xnox sent [email that dopped Ubuntu desktop x86-32 bit](https://lists.ubuntu.com/archives/ubuntu-release/2017-September/004212.html)  [Phornix post - Ubuntu 17.10 Will Drop The 32-bit  Desktop ISO](https://www.phoronix.com/scan.php?page=news_item&px=Ubuntu-17.10-Drops-i386-ISO) |
| 2017-12-22 | Nvidia announces EOL for 32-bit - security updates only until January 2019. [NVIDIA To Stop Offering 32-bit Driver Support](https://www.phoronix.com/scan.php?page=news_item&px=32-bit-NVIDIA-Drop-Dropping).  [Nvidia Bulletin (subject to change)](https://nvidia.custhelp.com/app/answers/detail/a_id/4604/) |
| 2018-05-09 | Joint proposal to [drop images and port](https://lists.ubuntu.com/archives/ubuntu-devel-discuss/2018-May/018004.html) with [Simon Quigley](https://www.tsimonq2.net/) |
| 2018-05-10 | Phoronix post - [Ubuntu Developers Once Again Debate Dropping i386 Images, Then Discontinuing i386 Port](https://www.phoronix.com/scan.php?page=news_item&px=Ubuntu-i386-Dropping-Discussion) |
| 2019-06-18 | [i386 architecture will be dropped starting with eoan (Ubuntu 19.10)](https://lists.ubuntu.com/archives/ubuntu-devel-announce/2019-June/001261.html) |

# Current Status

i386 Arch is being dropped in 19.10.

Wine plans: Unknown

Steam plans: Unknown

| Ubuntu Flavor &nbsp; &nbsp;| Gone     | Decision Date/Thread                                                          |
| --            | ---      | ---                                                                                        |
| Desktop       | 17.10    | [2017-09-27](https://lists.ubuntu.com/archives/ubuntu-release/2017-September/004212.html)  |
| Server        | 18.04    | [2017-12-20](https://lists.ubuntu.com/archives/ubuntu-release/2017-December/004257.html)   | 
| Budgie        | 18.10    | [2018-05-04](https://ubuntubudgie.org/blog/2018/05/04/18-10-and-beyond-64bit-images-only)  |
| Mate          | 18.10    | [2018-05-04](https://ubuntu-mate.community/t/ubuntu-mate-18-10-dropping-i386-images/16715) |
| Studio        | 18.10    | [2018-05-06](https://lists.ubuntu.com/archives/ubuntu-release/2018-May/004466.html)        |
| Kylin         | 18.10    | [2018-05-14](https://lists.ubuntu.com/archives/ubuntu-release/2018-May/004477.html)        |
| Kubuntu       | 18.10    | [2018-05-16](https://lists.ubuntu.com/archives/kubuntu-devel/2018-May/011657.html)         |
| Xubuntu       | 19.04    | [2018-12-02](https://lists.ubuntu.com/archives/ubuntu-release/2018-December/004647.html)   |
| Lubuntu       | 19.04    | [2018-12-20](https://lubuntu.me/sunsetting-i386/)                                          |
| netboot       | 19.10    | 2019-06-18 Killing Arch                                                                  |
| ubuntu-base   | 19.10    | 2019-06-18 Killing Arch                                                                  |
| cloudimg      | 19.10    | 2019-06-18 Killing Arch                                                                  |

Flavor lists can be found on [cdimage.ubuntu.com](cdimage.ubuntu.com) and [https://cloud-images.ubuntu.com](https://cloud-images.ubuntu.com)

# Future things of interest
* [How to Removing the i386 archive from machines](https://unix.stackexchange.com/questions/295241/dpkg-error-cannot-remove-architecture-i386-currently-in-use-by-the-database#295243f)
* [wine platform in snap for packaging Windows apps as snaps](https://snapcraft.io/wine-platform-runtime)
