<!--
.. title: Climate extra
.. slug: climate extra
.. date: 2021-11-06 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. datatable: true
-->

This is a list of items we could invest or tax to help fight climate change . 


# Invest in the future {:#invest}
Most climate plans cover subsidizing business models or specific ventures like Solar or Electric cars. Many of those subsidies are not remotely equitable. Subsidies can raise the market price of a product - actually making it more expensive like what happened to converter boxes during the [DTV transition](https://web.archive.org/web/20090326011627/http://techpolicyinstitute.org/files/the_dtv_coupon_program.pdf).

Instead, we should invest in items that will pay dividends to the public far into the future.

## Local community funding {:#local}
Grant determination based on the expected impact with a priority to help those in lower income communities. All programs are Opt-in by local communities.

This could include funding for:

 * Carbon free energy generation in locations within 5 miles of high population 
 * Block by block neighborhoods voting to remove fossil gas (or other carbon based fuel) infrastructure. Funding renewables to replace the power generate and new electric appliances,
 * Composting / recycling / water improvement funds for municipalities
 * [Road diets](https://en.wikipedia.org/wiki/Road_diet) with an emphasis on >50% priority space for areas separated from car traffic

## 🥗 Equitable School Lunch 🍅 🥕 {:#schoollunch}

There [are](https://www.pcrm.org/news/blog/californias-new-plant-based-school-lunch-bill-healthy-kids-and-climate) [many](https://www.leeds-live.co.uk/news/leeds-news/leeds-school-dinners-climate-change-17558839) [many](https://www.theguardian.com/education/2019/may/17/schools-should-have-one-meat-free-day-a-week-says-charity) schools making their lunch plans more climate friendly by switching away from meat and dairy to great plant-based options. Many are also [sharing](https://www.timesunion.com/renew/education/article/School-deal-with-vegan-food-learning-curve-14995496.php ) what works to get kids to eat healthier. These should be encouraged and funded.

[Friends of the Earth](https://foe.org/resources/scaling-healthy-climate-friendly-school-food/) mention the racial equity issues and I think they deserve extra emphasis:

 * "Demand for plant-based milk alternatives is also high, especially among minority students. Approximately 95 percent of Asians, 60 to 80 percent of African Americans, 80 to 100 percent of American Indians and 50 to 80 percent of Hispanics are lactose intolerant. Offering more plant-based milk options is critical given that these groups represent a significant portion of public school students."
 * "USDA regulations assert that milk is not an optional, but a required component of a reimbursable school meal."

Lactose/dairy issues can be delayed hours or even days. This delay means the culprit may not be apparent to anyone. This can cause kids significant symptoms that impact their ability to do well in school.
At best, it just increases the food waste of that district as kids *have* to take the milk so the school gets federal funding for the lunch program.

## 🚀 Space Exploration 🛰️🪐 {:#explorespace}
[Rewiring America's](https://www.rewiringamerica.org/) introduction ends with "Billionaires may dream of escaping to Mars, but the rest of us ... we have to stay and fight.".

On the contrary, those going to the Moon, Mars, or exploring other planets with robots are fighting for all of us. If there is going to be a technology breakthrough that's going to help us fight or better understand climate change, I'd bet it will come from our space programs. Thanks to multiple space launch companies exploring reusable rockets we can actually set our sights higher for what we hope to achieve. Perhaps we can learn something that will help us from other planets or human spaceflight.

There are 7 solar systems bodies with atmospheres we might want to study that we are not continuously monitoring today - Mars, Venus, Jupiter, Saturn, Uranus, Neptune, and Titan. Science works best when it has many examples of something. If we want to do better storm or climate prediction we need to give more samples for scientists to study.

In most human exploration plans we have to use renewable energy (mostly sunlight) to power our space exploration. We have to use the available resources incredibly efficiently. A technology developed to utilize the CO2 martian atmosphere might be critical for the mission success, while it's Earth offshoot could enable carbon capture technology to work.

NASA has always been one of the best [investments](https://www.nasa.gov/sites/default/files/files/SEINSI.pdf) the US makes. [NASA Spinoff technologies](https://en.wikipedia.org/wiki/NASA_spinoff_technologies) include better GPS, infrared thermometers, LASIK, and firefighting equipment among thousands of others. While we can't be sure more funding for space will come up with a breakthrough, we can be sure that an active space program will inspire more people to become scientists and engineers.

## 🔌 "Smart" Grid ⚡ {:#smartgrid}
Connected or "Smart" cars can be very convenient, but they also introduce a series of risks we need to avoid when creating a smart grid. Some car features are software locked, meaning they could be taken away from consumers at any time. They are also dependent on that car company keeping services online for the full experience people are used to. This can result in cars becoming partially obsolete even if the physical car is fine. 

It is critical that any "Smart grid" setup treats consumers as the only one their devices answer too and never depends on services. Ideally this means

 * The source code is free and open source software and allows consumers the full capability to modify them. Devices that the consumer should not reasonably be able to modify such as the electric meter's source code is still made open so that customers (or more likely) experts can verify what they are doing.
 * There are no third parties needing data access
 * Consumer privacy is protected by segmenting what data needs to be sent all the way to their utility.
 * A non-profit or other mission driven organization controls the source code and makes updates available through the grid itself. We need to make sure any "smart" grid devices keep working securely for their full lifetime.
 
"Smart" grid devices should be every device that has an ability to modify its energy use to match grid conditions.  This includes HVAC systems, refrigerators, hot water heaters, and any computing devices. The grid capabilities must be available in all models, not just those marketed to be "smart" for other reasons. As such, the cost needs to be very low for including this functionality.

Due to climate change hurricanes, wildfires and other disasters will become more destructive so we need to be able to recover faster. We should aim for a grid that can self-regulate if it is cut off from other energy sources. This would need to use local renewable energy and local battery or other energy storage.

## Science driven lighting at night {:#lighting}
Lighting at night is primarily powered by either Coal or Nuclear, as those are still the US base load providers. It is expensive to start/stop coal and Nuclear power plants so they actually subsidized us all putting in way too much nighttime lighting then we needed. We need to re-evaluate how much lighting we actually need at night following [basic guidelines](https://www.darksky.org/our-work/lighting/lighting-for-citizens/lighting-basics/).

One estimate is that lighting takes up [5%](https://www.eia.gov/tools/faqs/faq.php?id=99&t=3) total U.S. electricity consumption (this includes residential and doesn't include commercial, which I'm counting as about the same). If we can reduce our lighting use by 20% that would lower the entire U.S. electricity consumption by 1%. More than that, it's at a time when we don't have solar to help us so it will more than likely help displace coal. For reference, that is about the same amount of electricity from all the [solar panels we've installed on our homes](https://www.eia.gov/tools/faqs/faq.php?id=427&t=3).



# Other items we could tax {:#otherstotax)

There are many other items we could try taxing under this tax capability. It could also be modified to try to minimize 1-time use items or packaging.

## Services

### Taxi services

### 🛫 Flying in Airplanes ✈️ 🛬 {:#airplanes}

Will use two examples for [example 2020 airfares](https://thepointsguy.com/news/2020-airfare-trends/):

 * Domestic at $250 a flight
 * International at $800

Tax amount, assuming steady prices

|                        | Year 10  | Year 20   |
| :-------------         | :------: | --------: |
| Domestic Flights       | $ 25     |  $ 50     |
| International Flights  | $ 80     |  $ 160    |

This is a great example of how equity in this plan is achieved. While in 10 years the economy tax has gone up $25. If we look at first class tickets as being usually 4x more expensive at $1000 they would pay in a full $100 at 10 years.

New Airplanes would also be taxed under new combustion devices (although not discussed separately). The combination of both should result in substantially more investments in alternative options for air travel.  Currently the best estimate I could find from 2019 was that investments in electric aircraft are in the [$125 million](https://www.investopedia.com/why-electric-airplanes-are-about-to-take-off-4706531) a year range. We need investments from industry to be in the billion range.


### 🐄 Ruminantia 🐐🐑 {:#ruminantia}
Ruminant animals produce methane in how they digest food ([Wikipedia](https://en.wikipedia.org/wiki/Ruminant#Ruminants_and_climate_change). Beef and Lamb are two such animals.
They are also commonly seen to be two of the most negatively impacting foods. (Chart from [EWG](https://www.ewg.org/meateatersguide/a-meat-eaters-guide-to-climate-change-health-what-you-eat-matters/climate-and-environmental-impacts/)).

 ![green_house_gas_protein](/images/2020-climate/ewg_green_house_proteins.webp)

[Example 2020 prices](https://www.vox.com/future-perfect/21366607/beyond-impossible-plant-based-meat-factory-farming):

 * Plant-based meat: $10 a pound
 * Beef: $5 a pound
 
Tax amount, assuming steady prices

 * 10 year: 50 cents
 * 20 year: $1 dollar

This should not have a significant impact on consumers, while having a huge impact on businesses that sell meat. Encouraging them to sell more of other meats or plant based meat.

Why not include this by default? A $1 increase over 20 years appears to be substantially less significant then when the plant-based meat companies are hoping to be cost competitive. It may make more sense to focus on cutting subsidies for meat and making other regulatory changes to make the playing field more even.


#### Digital Restrictions Management 🎞️🔒 {:#drm}
Many devices and services use digital restrictions to attempt to stop people from copying media. This would tax both devices and services that include or utilize DRM. Consumer DRM is known to be ineffective but big media companies don't want to give up their control over consumer devices. All the costs of DRM in wasted silicon or wasted electricity are paid by consumers. DRM may have a significant impact during the 5-10pm time period where more people are home using streaming services. At least in California, that is also when electricity is generally the dirtiest.

There are many alternative technologies like watermarking that allow consumers to save energy and keep control of their devices.