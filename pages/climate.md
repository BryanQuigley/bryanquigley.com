<!--
.. title: Climate
.. slug: climate
.. date: 2021-11-06 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. datatable: true
-->

# 1% more for the climate every year

Norway has been [incredibly successful](https://www.greencarreports.com/news/1123160_why-norway-leads-the-world-in-electric-vehicle-adoption) at increasing the market share of electric vehicles by making them more affordable using both taxes and incentives. Taxing ICE vehicles more heavily is a strategy that should be adopted. That can be expanded to cover all new carbon infrastructure from cars to rockets. This should also be more equitable than a carbon tax as it would just tax those who are purchasing new vehicles, devices or infrastructure.

I propose we create a Climate Negative Tax (CNT) targeting goods that negatively impact the climate. This tax would be charged as a business income tax on the gross sales of Climate Negative goods sold. The tax would grow an additional 1% every year to accelerate more changes every year, but giving the market the freedom to implement the most cost-effective solutions.

This tax does not negate the need to stop subsidizing fossil fuels, dairy/meat, or the need for large government investment. For certain items this tax provides a good transition (and builds acceptance) to eventually banning. 

Over 10 years, the CNT will be at 10%. The intention is to not cap the CNT, as we need most of these products or services to cease completely over the coming decades.
        
**Business Impact**

 * By impacting margins, we guarantee the company will take notice.
 * Create a much cleaner metric on just how irresponsible a corporation is when it comes to climate change.
 * Change of long-term strategic planning to be more sustainable.
 
**Consumers**

 * Hard to notice for consumers so it's less likely to meet resistance.
 * By being relative to the activities original cost it will be more progressive than other tax schemes.
 * It's challenging to promote sales with a 1% annual increase in costs. We must avoid scenarios where a significant tax hike day could inadvertently boost fossil fuel sales.
 * It's at the point where consumers are making the critical decisions that can make an impact. Unlike a carbon tax, where the consumer generally doesn't get a choice to fill up their gas car with wind energy.
 
**Possible Exceptions**

 * If your sector's processes effectively sequester carbon emissions equivalent to those produced.
 * If you are using waste sources from an otherwise non-taxed source. For example, if you are capturing methane from food waste.
  
 **Penalties**
 
 * If discovered by external investigations into a company, penalties should be charged at the standard [IRS rates](https://www.irs.gov/newsroom/interest-rates-remain-the-same-for-the-second-quarter-of-2020) from the time the original tax should have been paid.
 * If a company discloses an issue within 2 years or a device needs to be converted from an exception class to a taxed class it just needs to be taxed at what the original tax rate would have been using the original sale price (no deprecation). 

**Climate Negative Activities**

There are many [other items](#otherstotax) we could consider taxing like this, but for simplicity let's just focus on the two critical ones:

 * Fossil fuel infrastructure - Tax new pipelines, new hookups, or any new purchases of parts needed to maintain existing fossil fuel infrastructure.
 * Combustion Devices - New purchases (new vehicles).
 
Now, let's examine some specific scenarios.

### 🚘 New car purchase 🚗 🚙 {:#car-purchase}

Many car companies are talking about their last combustion model releasing in the 2025-2030 time-frame. We can accelerate that. Ideally, this results in canceling of planned combustion vehicles or switching to clean vehicles design instead. Electric car prices are also steadily decreasing as more players get into the market.

Example 2020 Prices:

 * Combustion car: 25k
 * Electric car: 35k

Tax amount, assuming steady prices. The overall [margins](https://www.cnbc.com/2016/04/28/ford-motor-sets-records-for-profit-operating-margins.html) for vehicles appear to be somewhere in the 10% range.

 * 1 years: .25k (Remaining profit 90% 2.25k)
 * 5 years: 1.25k (Remaining profit 50% 1.25k)
 * 10 years: 2.5k (Remaining profit 0% 0k)

Halving profit by 2025 will not be acceptable and they will have to move to clean vehicles then they were originally planning. Of course, it is also very likely the price will be increased instead of hurting margins, which would make combustion models less competitive. 

### 🚀 Rockets 🚀 {:#rockets}

Rockets are one of the items we can do sustainably under the track the carbon cycle concept. The fuels would have to be reduced to either methane or hydrogen dropping RP1 (refined kerosene similar to jet fuel) and solid rocket boosters. Both methane or hydrogen (which burns to water) can be produced by renewable energy allowing a carbon sink in the same process as launching the rocket. [Everyday Astronaut](https://everydayastronaut.com/rocket-pollution/) has a great video into rocket fuels and their climate impact.

 * Example launch cost: $60 million.
 * Example fuel cost: $300 thousand.
 * Simple Year 3 tax (at triple the fuel cost): $900 thousand.

I fully expect the rocket industry that can shift to use either waste gas or generating it from renewable resources themselves.

# Conclusion {:#conclusion}

I hope I've convinced you that a Climate Negative Tax targeting climate change activities should be further evaluated as part of what we need to do to fight climate change. If not, please leave feedback as to why I didn't convince you by opening an [issue here](https://gitlab.com/BryanQuigley/bryanquigley.com/-/issues).