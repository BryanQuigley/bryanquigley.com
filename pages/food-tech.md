<!--
.. title: Food is a technology
.. slug: food-tech
.. date: 2019-12-01 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. datatable: true
-->

My son and I tried some plant-based products (both meat and dairy replacements).  Primarily doing this for the planet/climate.

## Meat

### Nuggets

**Best**
Yummy Dino Buddies Meatless Veggie Nuggets

**Good**

 * Trader Joes Meatless Nuggets
 * Gardein nuggets
 
**OK** Simple Truth


### Ground

**Good** Impossible ($9, seen for $7!). Both of us prefer it to our previous ground turkey, by a large margin

**OK** 
 * Beyond Meat ($9). Good, but still have slight preference for the ground turkey (range from $2-$7) we usually get
 * Kroger's Simple Truth Emerge Plant-Based Grind ($7). Pea based protein - still prefer how Impossible cooks and tastes.  Alec had seconds. I didn't like it very much the second time.


**Meh** Trader Joe's Beef-less Ground beef ($4 for 8oz). OK for me, but son didn't like it very much.

**Avoid**

 * MorningStar burrito ground I don't remember the exact one, but it was inedible for both of us.
 * Pure Farmland Simply Seasoned Plant-Based Protein Starters

### Burger
**Avoid** Tried Impossible Burger at Ruby's, not impressed.  In-N-Out across the street sells better burgers for ~$3. Not worth it.  Ruby's was $14.
**OK** Impossible Burger at local restaurant V Burger (all plant based).  Son ate about half and didn't like it very much. I think it's the plant-based cheese at fault but am unsure.

### Meatballs
**Good** Gardien Classic meatless meatballs - son thinks they are meat.
**Meh** Trader Joes's Meatless meatballs - ok for me or in a pinch.

### Sausauge / Bacon

**Meh** Field Roast Grain Meat Co.  Breakfast Suasauge.  Apple Maple.  Wasn't bad, but not likley to do again.
**Meh** Lightlife Smart Bacon. Was edible, but would not do again.
**Avoid** Beyond Meat Sausage ($9). Cooked like Kielbasa (may not be recommended). We both ate it, but wouldn't do it again. Described as "rotted chicken nuggets".

### Asian?
**Just for me** Gardein sweet and source porkless bites. Delicious for me, to spicy for my son.

### Deli style

**Great** Simple Truth Plant Based Ham Style Slices Black Forest. Awesome toasted with Follow your heart parm.

**Meh** Quorn Meatless Turkey-Style Deli Slices. Wouldn't do a again, but wasn't bad.

### Crab Cakes
**Good** Trader Joe's Vegan Jackfruit Cakes

## Meals
**Good** Simple Truth Tofu Pad Thai

## Dairy

## Ice Cream
**Great**
 * Simple Truth™ Peanut Butter Chip Oatmilk Non-Dairy Frozen Dessert
 * Ben and Jerry's "Milk" and cookies

Would like bigger containers though..

**Good**
Trader Joes Soy based ice cream, which was good to make shakes with. Not as good by itself.

### Sour Cream
**Good** Tofutti Better Than Sour Cream $3.99 (soy based) - great to make a chip dip with too!

**OK** Simple Truth Organic™ Non-Dairy Sour Cream $2.79 ($1 coupon) - butter bean based, caused some minor digestive issues.  Tastes great.

### Cream Cheese

**Good** Daiya Strawberry Cream Cheese Style Spread  4.49 (50c coupon)

**OK** Simple Truth Organic™ Plant-Based Plain Cream Cheese Alternative butter bean based, caused some minor digestive issues. Tastes great.

### Pizza
**Good** ZPizza Berkeley Vegan

**Meh** Daiya Thin Crust Gluten-Free Pizza.  Cheeze Lover's. Would not buy again - best part was the cheese, but it was OK.

### Cheese
**Great** Follow Your Heart Dairy Free Parmesan Style Shredded Cheese Alternative
**Good** Daiya American Style slices.
**Meh** Simple Truth Plant Based Non-dairy Cheddar slices (tastes more like American..)



## Snacks
**Great** From the Ground Up Cailiflower Stalks - Cheddar Flavor (Fully Vegan)


## Other -
Mountain house - says 10 servings.. but we ate 1/2 of it for 1 meal - so more like 4-6 servings for us... Makes it totally not worthwhile.

Sow Chia non-dairy beverage Vanilla Unsweetened. Meh for cereal. not good for drinking seperate
Ripple Milk (pea based) - Fine, but expensive