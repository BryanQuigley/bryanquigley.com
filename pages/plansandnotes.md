# Policy

Proposals I've written up that are still current.

 * [Climate](climate.html)
  * [Dark Sky | Lighting at night](darksky.html)

# Trackers
 * [Ubuntu dropping i386 tracker](ubuntu-drop-i386.html)
 * [Food is a technology](food-tech.html) - My personal log of foods I like, with a focus on plant-based.
 * [MPEG-2 Patents](mpeg2-patent-tracker.html)

# Older

 * [History](random-blog-history.html)
 * Starship Congress 2015 - Software on a Manned Interstellar Expedition [Presentation](https://docs.google.com/presentation/d/1RZEQCyCmX_mfV4VCjQFVyImHMQB4F-7m_TsAVWg6OHM/edit?usp=sharing) [Draft Paper](software-on-a-manned-interstellar-expedition.html) [YouTube](https://www.youtube.com/watch?v=rqBKqNz1P4U])
 * [End the corn subsidy](https://web.archive.org/web/20150911065439/https://endcornsubsidy.wordpress.com/)
 * [Ubuntu Monthly Update Cadence proposal](ubuntu-monthly-update-cadence.html)
