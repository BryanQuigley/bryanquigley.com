<!--
.. title: Dark Sky
.. slug: darksky
.. date: 2020-08-09 07:00:00 UTC
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. datatable: true
-->

Lighting at night 

 * helps make Coal power economical (base load requirements)
 * has not been shown to help safety
 * has not been shown to reduce accidents
 * 

[http://www.darksky.org](http://www.darksky.org) [http://news.discovery.com/animals/bugs-streetlights-120522.html](http://news.discovery.com/animals/bugs-streetlights-120522.html) [http://cs.astronomy.com/asy/b/astronomy/archive/2015/07/29/humans-cling-to-their-primal-fear-of-the-dark.aspx](http://cs.astronomy.com/asy/b/astronomy/archive/2015/07/29/humans-cling-to-their-primal-fear-of-the-dark.aspx)

https://www.aaihs.org/a-meditation-on-natural-light-and-the-use-of-fire-in-united-states-slavery/
https://www.aaihs.org/race-and-the-paradoxes-of-the-night/
https://www.darksky.org/toward-an-equitable-dark-sky-movement/


lighting-
https://www.independent.co.uk/news/uk/home-news/turning-off-street-lights-does-not-cause-increase-in-traffic-accidents-or-crime-says-study-10421845.html
https://theconversation.com/the-science-of-street-lights-what-makes-people-feel-safe-at-night-103805
https://www.govtech.com/fs/How-Streetlights-Can-Bridge-Racial-Gaps-in-Cities.html
https://www.who.int/publications/i/item/world-report-on-road-traffic-injury-prevention


# Los Angeles County - 2020 Dark Sky Strategic Plan
Bryan Quigley, IDA delegate.  Aug 5, 2020.

Mission statement:  To increase dark sky organization and awareness in the most populous county in the US.  Help with the IDA website and other technical tasks.
## 1 – Create a LA dark sky home

Right now there is no local source of information about dark sky issue's in LA county.   The first step is to create one.

 * Establish a website just for local information/organization (Ideally make it easy for other non-chapter groups to do similar)
 * Establish communication medium for anyone who wants to get involved (Discourse, etc)
 * Fix DarkSky.org map so you can see California (and east of Australia, etc). 
 * Make introductory posts to environmental groups, craigslist, astronomy groups to find more who want to get involved
 
## 2 – Find other partners

 * Environmental groups like FossilFreeCA  have a clear connection against wasting power.  The state of California reports on the carbon impact of power during every day.  The peak time for carbon use by far is when all the lights go on 8pm (See caiso.com).
 * Astronomical groups/Griffith Observatory – Already have education mission.
 * Space Exploration: Planetary Society, Rocket companies – SpaceX/RocketLab/Virgin Galactic – all based in LA – need more people who want to reach for the stars (need stars to reach to).

## 3 - “Double the Stars”

Launch an outreach campaign to get more people thinking about how few stars they can see and how it wouldn’t be that difficult to double the number we can see at night.  Focus on all negative aspects of light pollution.  With more people behind the concept, we can reach out to focus on reduction goals.

 * Port of LA and Long Beach (biggest light polluter by far near me).  Parts of it are closed on certain days/times, but still use high-mast lighting due to security perception.
 * Investigate increasing cost of lighting (I submitted a letter to the recent power company rate proposal increase – they were increasing the lighting cost by much less than residential.  This was my first visit to the dark sky map – I didn’t successfully get in touch with anyone that time to spread the word)
 * Reach out to police forces about their advice they give around flood lights.  Two places I’ve reach out do locally have both said they were advised to use flood lights by police analysis of their buildings.
 * Propose light pollution ordinances for all LA County or individual cities.

The first 2, I’m already trying to do myself, but having more people involved can only help.  The last 2 I’d want a good number of people behind me.



SCE rate post
There is substantial missing context for this and unfortunately SCE's website has some dead links at the moment so I can't research it as much as I would like (I've emailed them for followup).  *** Especially concerning to me is the fact that Lighting is increased by the least in this rate proposal. Lighting at night can 1)  be harmful to health (especially shift workers or those in neighborhoods closer to industry - which is commonly people of color) 2) be harmful to ecosystems 3) be helpful to crime (contrary to popular belief it generally makes you less safe, while *feeling* more so, that makes people more vulnerable) *** Obviously, lighting happens when there is no solar power, which means we may be more dependent on fossil fuels to power them.   By prioritizing lighting over households we are keeping business and governments from re-evaluating if they need all the various lights they have today.  We need them to take a hard look and make sure all energy use is actually necessary and helpful. *** I'd like to see the next request include information on climate change and more information breaking down each rate class.  Some questions I’d like to see answered: 1) What is the carbon impact of each rate class?   2) or climate change, We need to replace all methane service in the medium term.  How does this help achieve this goal? 3) Has anyone looked at racial or class disparities this may make worse? *** Please do not approve as is. *** Related reading: https://en.wikipedia.org/wiki/Light_pollution#Consequences
https://www.aaihs.org/race-and-the-paradoxes-of-the-night/
