I decided to summarize some older posts instead of saving them going forward:

*   2008-05 - My Theory of Nothing, which was already thought of by Wheeler in [Geometrodynamics](https://en.wikipedia.org/wiki/Geometrodynamics)
*   2009ish - Was quite vocal against Mono being installed by default in Ubuntu
*   2009-02 - Proposed [no input recovery](https://wiki.ubuntu.com/Specs/no-input-recovery) for when either a mouse or keyboard is missing. (No longer interested in pursuing)
*   2009-03 - Tried to get Node 3 of the ISS names for Ubuntu.  Got it on the board, but Colbert has a bigger following.
*   2009-04 - Want to kill the screen-saver to save electricity (still do)
*   2009-08 - Another music player review
*   2011-02 - Got out of Facebook
*   2011-03 - Ran a Linux Gaming weekend to try to promote gaming on Linux
*   2011-05 - Was a happy Palm Pre (WebOS user)
*   2012-04 - Got first IPv6 Comments on Blog
*   2012-07 - 12.04 Music Player review - my top choices were Audacious, Clementine, gmusicbrowser, Guayadeque, Musique, Quod Libet, and Rhythmbox
*   2014-06 - Found out that we still put Lead in consumer products like Garden hoses, Power cords, Carseats, and more.

You can find remaining posts in the [Archives](../archive.html)
